
from canevas.collection import Collection
from pathlib import Path
import json


class Setting:
    def __init__(self, name='', set=None, get=None):
        self.name = name
        self.set = set
        self.get = get


class Settings(Collection):
    def __init__(self, path, *settings):
        super().__init__()

        self.path = Path(path).with_suffix('.json')

        for setting in settings:
            self.add(setting)

    def add(self, setting):
        if not isinstance(setting, (dict, Setting)):
            return

        if isinstance(setting, dict):
            setting = Setting(**setting)
        
        self[setting.name] = setting

    def norm_data(self, obj):
        if isinstance(obj, dict):
            new = {}
            for k, v in obj.items():
                new[k] = self.norm_data(v)
        elif isinstance(obj, list):
            new = []
            for v in obj:
                new.append(self.norm_data(v))
        elif isinstance(obj, Path):
            return str(obj)
        else:
            return obj

        return new

    def save(self, data=None):
        #print(f'Saving {self.path}')
        self.path.parent.mkdir(parents=True, exist_ok=True)

        if data is None:
            data = {s.name : s.get() for s in self}

        data = self.norm_data(data)

        self.path.write_text(json.dumps(data, indent=4), encoding="utf-8")

    def load(self):
        if not self.path.exists():
            return       
        return json.loads(self.path.read_text(encoding="utf-8"))

    def restore(self, data=None):
        data = data or self.load()

        if not data:
            return

        #try:
            #print(f'Restore {self.path}')
        for setting in self:
            value = data.get(setting.name)
            if value is not None:
                try:
                    setting.set(value)
                except Exception as e :
                    #print(f'Failed to namerestore values for {self.path}')
                    print(e)
        '''
        except Exception as e :
            print(f'Failed to restore values for {self.path}')
            print(e)
            '''