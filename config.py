
import yaml
import json
import os
from os.path import expandvars
from pathlib import Path


class Config:
    def __init__(self, config_file):
        #self.path = None
        self.data = self.read(config_file)

    @classmethod
    def read(cls, config_file, as_pathlib=True, expand_vars=True, obj_from_path=True):
        if not config_file: 
            return

        if isinstance(config_file, dict): 
            data = config_file 
        else: # It's a path
            print('Config Read', config_file)
            config_file = Path(config_file)

            if not config_file.exists():
                print(f'The Config file {config_file} does not exist')
                return
            
            #self.path = config_file
            os.chdir(config_file.parent)

            if config_file.suffix == '.yml':
                data = yaml.safe_load(config_file.read_text() )

            elif config_file.suffix == '.json':
                data = json.loads(config_file.read_text() )

        if not as_pathlib and not expand_vars:
            return data
        
        return cls.expand(data, as_pathlib, expand_vars, obj_from_path)

    @classmethod
    def expand(cls, obj, as_pathlib=True, expand_vars=True, obj_from_path=True):
        """
        Recursivly goes through the dictionnary obj and replaces value with / or \\
        into absolute Path.
        """
        if isinstance(obj, dict):
            new = {}
            for k, v in obj.items():
                new[k] = cls.expand(v, as_pathlib, expand_vars, obj_from_path)
        elif isinstance(obj, list):
            new = []
            for v in obj:
                new.append(cls.expand(v, as_pathlib, expand_vars, obj_from_path))
        elif isinstance(obj, str):
            if expand_vars and '$' in obj:
                obj = expandvars(obj)
            if any([i in obj for i in ('/', '\\')]):
                if obj.startswith('http') or not as_pathlib:
                    return obj
                if obj.startswith(('./', '../')):
                    obj = Path(obj).absolute().resolve()
                else:
                    obj = Path(obj)
                if obj_from_path and obj.parent.suffix == '.py':
                    return cls.obj_from_path(obj)
            return obj
        elif isinstance(obj, Path):
            if expand_vars:
                obj = Path(expandvars(str(obj)))
            if str(obj).startswith(('./', '../')):
                obj = obj.absolute().resolve()
            if obj_from_path and obj.parent.suffix == '.py':
                return cls.obj_from_path(obj)
            return obj
        else:
            return obj

        return new

    @staticmethod
    def obj_from_path(path) :
        from importlib import util

        path = Path(path)

        obj_name = path.name
        path = str(path.parent)

        spec = util.spec_from_file_location(obj_name, path)
        mod = util.module_from_spec(spec)

        spec.loader.exec_module(mod)

        return getattr(mod, obj_name)