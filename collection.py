


class Collection:
    def __init__(self, data={}):
        self._data = []
        self.data = data.items()

    @classmethod
    def from_attr(cls, attr, data):
        collection = cls()
        for d in data:
            collection[getattr(d, attr)] = d
        return collection

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, dict):
        for k, v in dict:
            self[k] = v

    def clear(self):
        self._data.clear()

    def to_dict(self):
        return {k: v for k,v in self.data}

    def items(self):
        return self.to_dict().items()

    def keys(self):
        return self.to_dict().keys()

    def __add__(self, other):
        col = Collection()
        col.data = self.data + other.data

        return col

    def __radd__(self, other):
        if other == 0:
            return self
        else:
            return self.__add__(other)

    def __setitem__(self, key, value):
        self._data.append((key, value))

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.data[key][1]
        elif isinstance(key, str):
            return self.to_dict()[key]

    def __delitem__(self, key):
        if isinstance(key, int):
            self.data.pop(key)

        elif isinstance(key, str):
            i = next(i for i,k in enumerate(self.data) if k[0] == key)
            self.data.pop(i)

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return (i[1] for i in self.data)

    def __repr__(self):
        text = '\nCollection(\n'
        for k, v in self.data:
            text+= f"    {k}: {v}\n"
        text+= ')\n'

        return text

    def get_by_id(self, id):
        #print(id, [v.id for k,v in self.items()])
        return {v.id: v for k, v in self.data }.get(id)

    def get(self, name, other=None):
        if name in self.keys():
            return self[name]
        
        return other

    def find_all(self, **kargs):
        return [i for i in self if all([getattr(i, k)==v for k, v in kargs.items() ]) ]

    def find(self, **kargs):
        result = self.find_all(**kargs)
        if result:
            return result[0]

    def index(self, item):
        return self._data.index(item)

    def last(self):
        if len(self):
            return self[-1]
