
from PySide2.QtWidgets import QApplication
from PySide2.QtGui import QColor

from .properties import PropertyGroup, EnumProperty, ColorProperty, \
IntProperty, PathProperty
from ..config import Config
from ..collection import Collection
from .stylesheet import StyleSheet

from pathlib import Path



class Themes(Collection):
    def __init__(self):
        super().__init__()

        self.root = None
        self.property = EnumProperty()
        self.property.value_changed.connect(self.apply)
        #self.stylesheet = StyleSheet.from_file(self.qt_app, self.css, properties=self.context.theme.properties)

    def apply(self, theme):
        self[theme].apply()

    def add(self, theme_file):
        theme = Theme(theme_file)
        self[theme.name] = theme

        self.property.add_item(theme.name)

    @classmethod
    def from_dir(cls, theme_dir):
        self = cls()

        self.root = Path(theme_dir)
        themes = [t for t in self.root.glob('*.*') if t.suffix in ('.json', '.yml', '.yaml')]

        for theme_file in themes:
            self.add(theme_file)
        
        return self
    

class Theme:
    def __init__(self, theme_file):

        self.name = ''
        self.stylesheet = None

        self.colors = PropertyGroup(
            background=ColorProperty(name='Background Color'),
            widget_base=ColorProperty(name='Widget Base Color'),
            widget_dark=ColorProperty(name='Widget Dark Color'),
            widget_very_dark=ColorProperty(name='Widget Very Dark Color'),
            widget_bright=ColorProperty(name='Widget Base Color'),
            text_disabled=ColorProperty(name='Text Color Disabled'),
            text_normal=ColorProperty(name='Text Color Normal'),
            text_highlight=ColorProperty(name='Text Color Highlight'),
            select_text_bg=ColorProperty(name='Selected Text Background'),
            hover=ColorProperty(name='Hover Color'),
            highlight=ColorProperty(name='Highlight Color'),
            background_highlight=ColorProperty(name='Background Highlight Color'),
            item_highlight=ColorProperty(name='Background Highlight Color')
        )

        self.borders = PropertyGroup(
            radius=IntProperty(name='Border Radius', min=0, max=10, decimals=1, unit='px'),
            width=IntProperty(name='Border Width', min=0, max=10, decimals=0, unit='px'),
        )
        
        self.icon_dir = PathProperty(name="Icon Directory")
        '''
        s = PropertyGroup(
            close=PathProperty(name='Close'),
            reset=PathProperty(name='Reset'),
            search=PathProperty(name='Search'),
            arrow_up=PathProperty(name='Arrow Up'),
            arrow_down=PathProperty(name='Arrow Down'),
            arrow_down_small=PathProperty(name='Arrow Down Small'),
            arrow_left=PathProperty(name='Arrow Left'),
            arrow_right=PathProperty(name='Arrow Right'),
            plus=PathProperty(name='Plus'),
            minus=PathProperty(name='Minus'),
            folder=PathProperty(name='Folder'),
            bin= PathProperty(name='Bin'),
            install= PathProperty(name='Install')
        )'''

        self.load(theme_file)

    @property
    def properties(self):
        return {**self.colors.dict(), **self.borders.dict(), 'icon_dir': self.icon_dir}

    def apply(self):
        app = QApplication.instance()
        app.theme = self
        self.stylesheet.set()

    def save(self):
        self.display['ui_scale'].value_changed.connect(self.scale_ui)

    def load(self, theme_file):
        config = Config.read(theme_file)

        self.filepath = Path(theme_file)
        self.name = self.filepath.stem

        #self.stylesheet_file.value = config['stylesheet']

        for key, prop in self.colors.items():
            value = config['colors'].get(key)
            if value is not None:
                #print(key, value, len(value))

                prop.value = QColor(*value)

        for key, prop in self.borders.items():
            value = config['borders'].get(key)
            if value is not None:
                prop.value = int(value)
                #print(key, value, prop._value)

        self.icon_dir.value = Path(config['icon_dir'])
        '''
        for key, prop in self.icons.items():
            value = config['icons'].get(key)
            if value is not None:
                prop.value = Path(value)

                #print(key, value, prop._value)
        '''

        self.stylesheet = StyleSheet.from_file(self, config['stylesheet'], properties=self.properties)