
import sys

from .base_widgets import *

from PySide2.QtWidgets import QSizePolicy, QLineEdit, QColorDialog, QFileDialog, \
QGraphicsOpacityEffect, QGraphicsDropShadowEffect
from PySide2.QtCore import Qt, Signal, QObject
from PySide2.QtGui import QColor, QIcon, QTextCursor, QTextCharFormat, QBrush


class OutputText(QObject):
    updated = Signal(str)

    def __init__(self, connect=None):
        super().__init__()

        if connect:
            self.updated.connect(connect)

    def write(self, text):
        sys.__stdout__.write(text)
        self.updated.emit(str(text))

    def flush(self):
        sys.__stdout__.flush()

    def __del__(self):
        sys.stdout = sys.__stdout__


class OutputError(QObject):
    updated = Signal(str)

    def __init__(self, connect=None):
        super().__init__()
        if connect:
            self.updated.connect(connect)

    def write(self, text):
        sys.__stderr__.write(text)
        self.updated.emit(str(text))

    def flush(self):
        sys.__stderr__.flush()

    def __del__(self):
        sys.stderr = sys.__stderr__


class Console(PlainTextEdit):
    def __init__(self, parent=None):
        super().__init__(parent, readOnly=True, focus='NoFocus',
        tag='Console', Background='very_dark')


        self.setMinimumHeight(35)
        self.setViewportMargins(5, 2, 2, 0)
        #self.setCenterOnScroll(True)
        #self.resize(200, 35)
        sys.stdout = OutputText(connect=lambda x: self.add_text(x, QColor(210, 210, 210)))
        sys.stderr = OutputError(connect=lambda x: self.add_text(x, QColor(195, 75, 30)) )

    def add_text(self, text, color=None):
        cursor = self.textCursor()

        if color:
            format = QTextCharFormat()
            format.setForeground( QBrush( QColor( color ) ) )
            cursor.setCharFormat( format )

        cursor.insertText(text)
        cursor.movePosition(QTextCursor.End)

        self.setTextCursor(cursor)
        self.ensureCursorVisible()

    def resizeEvent(self, e):
        super().resizeEvent(e)
        self.ensureCursorVisible()
        #self.verticalScrollBar().setValue(self.verticalScrollBar().maximum() )


class Thumbnail(ImageLabel):
    def __init__(self, parent, image=None, size=(48, 32) ):
        super().__init__(parent)

        self.setFixedSize(*size)
        self.setPixmap(image)

    def setPixmap(self, image):
        pix = None
        if isinstance(image, (str, Path) ):
            image_path = Path(image)
            if image_path.exists():
                pix = QPixmap(image_path.as_posix())
        elif isinstance(image, QImage):
            pix = QPixmap.fromImage(image)

        if pix:
            pix = pix.scaled(self.size(), Qt.KeepAspectRatioByExpanding, Qt.SmoothTransformation)
            super().setPixmap(pix)
        else:
            super().setPixmap(QPixmap())


class PanelHeader(HWidget):
    pressed = Signal()
    def __init__(self, parent, icon=None, title=None, tag=None, **kargs):
        super().__init__(parent=parent, size=(-1, 26), spacing=2, margin=(2, 0),
        icon=None, Background='dark')

        self.collapseIcon = WidgetIcon(self, icon='arrow_down_small')
        if icon:
            self.icon = WidgetIcon(self, icon=icon, size=(24, 24))
        self.title = TextLabel(self, text=title,
        FontWeight='normal', FontColor='medium' )

    def mousePressEvent(self, e):
        super().mousePressEvent(e)
        self.pressed.emit()
    

class Panel(VWidget):
    def __init__(self, parent=None, icon=None, title=None, margin=0, tag=None, **kargs):
        super().__init__(parent=parent, margin=margin, align='Top')

        self.panelHeader = PanelHeader(self, title=title, icon=icon)
        self.panelHeader.pressed.connect(self.collapse)
        self.collapsed = False

        self.body = VWidget(self, **kargs)

    def collapse(self):
        self.collapsed = not self.collapsed
        self.body.setVisible(not self.collapsed)

        icon = Icon('arrow_right_small' if self.collapsed else 'arrow_down_small')
        self.panelHeader.collapseIcon.setIcon(icon)


class IconButton(PushButton):
    def __init__(self, parent=None, icon=None, connect=None, cursor=None, focus='NoFocus',
    emboss=True, toolTip='', size=(28, None), checkable=False, tag=None, **kargs):
        super().__init__(parent=parent, icon=icon, connect=connect, focus=focus,
        cursor=cursor, toolTip=toolTip, size=size, checkable=False, tag=tag, **kargs)

        self.hover = False


class MenuButton(PushButton):
    def __init__(self, parent=None, icon=None, menu=None, connect=None, cursor=None,
    toolTip='', size=None, tag='Menu', **kargs):
        super().__init__(parent=parent, icon=icon, connect=connect,
        cursor=cursor, toolTip=toolTip, size=size, tag=tag, **kargs)

        if menu:
            self.setMenu(menu)
            

class WidgetIcon(PushButton):
    def __init__(self, parent=None, icon=None, connect=None, focus='NoFocus',
    cursor=None, toolTip='', checkable=False, size=(18, 18), tag=None, **kargs):
        super().__init__(parent=parent, icon=icon, connect=connect, focus=focus,
        cursor=cursor, toolTip=toolTip, size=size, checkable=False, tag=tag, **kargs)

        self.hover = False

        self.setAttribute(Qt.WA_TransparentForMouseEvents)

        #self.setFixedSize(16,16)
        #self.setAttribute(Qt.WA_TranslucentBackground, True)
        #self.setWindowOpacity(0.35)
        '''
        self.opacityEffect = QGraphicsOpacityEffect(self)
        self.setGraphicsEffect(self.opacityEffect)
        self.opacityEffect.setOpacity(0.75)

    def enterEvent(self, e):
        super().enterEvent(e)
        self.hover = True
        self.opacityEffect.setOpacity(1.0)
        
        #self.setAutoFillBackground(True)
        #self.setResetBtn()
        #self.rightButton.show()
        #self.leftButton.show()

    def leaveEvent(self, e):
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        super().leaveEvent(e)
        self.hover = False
        self.opacityEffect.setOpacity(0.75)
        #self.setResetBtn()
        #self.rightButton.hide()
        #self.leftButton.hide()
        '''




class WidgetButton(PushButton):
    def __init__(self, parent=None, text='', icon=None, connect=None, focus='NoFocus',
    toolTip='', checkable=False, tag=None, size=(18, 20), **kargs):
        #kargs = {' **kargs}

        super().__init__(parent=parent, text=text, connect=connect,
        cursor='ArrowCursor', size=size, toolTip=toolTip, checkable=False, 
        focus=focus, tag=tag, **kargs)

        self.hover = False

        icon = Icon(icon)

        self.setIcon( icon )

        '''
    def enterEvent(self, e):
        super().enterEvent(e)
        self.hover = True

        pixmap = self.defaultIcon.getActivePixmap(shadowColor=QColor(0,0,0,220))

        #pixmap = self.defaultIcon.getOverlay()
        self.setIcon(pixmap)
        #self.setIcon(self.defaultIcon)

    def leaveEvent(self, e):
        super().leaveEvent(e)
        self.hover = False

        #self.defaultIcon.setNormal()
        self.setIcon(self.defaultIcon)

        '''


class StringLineEdit(HWidget):
    cursorPositionChanged = Signal(int, int)
    editingFinished = Signal()
    inputRejected = Signal()
    returnPressed = Signal()
    selectionChanged = Signal()
    textChanged = Signal(str)
    textEdited = Signal(str)

    def __init__(self, parent=None, text='', name='', nameVisible=True, icon=None, echoMode='Normal',
    clearBtn=False, reset=False, connect=None, tag=None, size=None, **kargs):
        super().__init__(parent, name=name, nameVisible=nameVisible, size=size)

        self.reset = reset
        self.defaultText = text
        self.resetButton = None
        self.icon = None
        self.clearButton = None

        kargs = {k.replace('Label', ''): v for k,v in kargs.items() if k.startswith('Label') }
        self.label = TextLabel(self, sizePolicy=('Fixed', 'Fixed'), **kargs)
        self.setName(name)

        self.setNameVisible(self.nameVisible)

        #self.frame = HWidget(self, tag=(tag or '')+'LineEdit', **kargs)
        self.lineEdit = LineEdit(self, tag=tag, **kargs)

        self.frame = HLayout(self.lineEdit, margin=4)
        
        self.icon = WidgetIcon(self.frame)
        self.setIcon(icon)
            
        self.lineEdit.addStretch()

        self.buttonFrame = HWidget(self.frame, spacing=1)

        self.clearButton = None
        if clearBtn:
            self.clearButton = WidgetButton(self.buttonFrame, icon='close', Width=16)
            self.clearButton.hide()
            self.clearButton.clicked.connect(lambda: self.lineEdit.clear())

            self.textChanged.connect(self.setClearButton)

        self.resetButton = None
        if reset:
            self.resetButton = WidgetButton(self.buttonFrame, icon='reset', Width=14,
             connect=lambda: self.lineEdit.setText(self.defaultText) )
            self.resetButton.hide()
            self.textChanged.connect(self.setResetButton )

        if connect:
            self.textChanged.connect(connect)

        self.lineEdit.setEchoMode(getattr(QLineEdit, echoMode))

        self.lineEdit.cursorPositionChanged.connect(self.cursorPositionChanged)
        self.lineEdit.editingFinished.connect(self.editingFinished)
        self.lineEdit.inputRejected.connect(self.inputRejected)
        self.lineEdit.returnPressed.connect(self.returnPressed)
        self.lineEdit.selectionChanged.connect(self.selectionChanged)
        self.lineEdit.textChanged.connect(self.textChanged)
        self.lineEdit.textEdited.connect(self.textEdited)

        '''
        items = (
            'fruit pomme', 'couleur rouge',
            'fruit orange', 'couleur vert',
            'fruit banane','couleur bleu')

        if completer: 
            self.completer = ListCompleter(self.lineEdit, parent=self.frame, items=items)

            '''
    def setIcon(self, icon):
        if not icon:
            self.lineEdit.styleProperties['LPadding']=0
            return
        
        self.icon.setIcon(Icon(icon))
        self.lineEdit.styleProperties['LPadding']=20

        #print('Set icon to', self)


    def setNameVisible(self, visible):
        self.label.setVisible(bool(visible))

    def setName(self, name):
        self.label.setText(name)
        #self.label.setVisible(bool(name))

    def name(self):
        return self.label().text()

    '''
    def setSeparator(self):
        if not self.clearButton and self.resetButton:
            return self.separator.setVisible(False)
        if self.resetButton.isVisible() and 
        self.resetButton.setVisible(self.reset and self.text() != self.defaultText )
    '''

    def setResetButton(self):
        self.resetButton.setVisible(self.reset and self.text() != self.defaultText )

    def setText(self, text):
        return self.lineEdit.setText(text)

    def text(self):
        return self.lineEdit.text()

    def setClearButton(self, text):
        self.clearButton.setVisible( bool(text) )


class EnumButtonGroup(QButtonGroup):
    def __init__(self, exclusive=True):
        super().__init__()
        self.setExclusive(exclusive)


class EnumButton(PushButton):
    def __init__(self, parent=None, text='', icon=None):
        super().__init__(parent=parent, text=text, icon=icon, 
        checkable=True, focus='NoFocus')

        self.isActive = False

    def mousePressEvent(self, e):
        self.isActive = True
        self.click()
        self.parent().activeState = self.isChecked()
        return 
        #self.click()
        #self.pressed.emit()
        

class ListEnum(HWidget):
    valueChanged = Signal(object)
    itemsChanged = Signal(object)

    def __init__(self, parent=None, items=[], exclusive=True, connect=None,
    sizePolicy=('Expanding','Fixed'), collapse=True, tag=None, **kargs):
        super().__init__(parent, sizePolicy=sizePolicy, collapse=collapse, **kargs)

        self.buttonGroup = EnumButtonGroup(exclusive=exclusive)

        if connect:
            self.valueChanged.connect(connect)

        self.buttonGroup.buttonClicked.connect(
        lambda: self.valueChanged.emit( self.value() ) )

        #self.itemsChanged.connect(self.onItemsChanged)

        self.setItems(items)

        self.activeState = None
    
    def mouseMoveEvent(self, e):
        for b in self.items():
            pos = b.mapFromGlobal(e.globalPos())
            if b.hitButton(pos):
                if not b.isActive:
                    b.isActive = True
                    b.setChecked(self.activeState)
            else:
                b.isActive = False
    
    def mouseReleaseEvent(self, e):
        self.activeButton = None
        self.valueChanged.emit( self.value() )
        super().mouseReleaseEvent(e)
    '''
    def onItemsChanged(self):
        for index, item in enumerate(self.items()):
            data = {}
            if index == 0: 
                data['RBorderRadius'] = 0
            elif 0 < index <= len(self.items()):
                data['LBorderWidth'] = 0
                data['LBorderRadius'] = 0
                data['RBorderRadius'] = 0
            else:
                data['LBorderRadius'] = 0

            item.styleProperties.update(data)
            '''
                   
    def selectAll(self):
        for i in self.items():
            i.setChecked(True)

    def items(self):
        return self.buttonGroup.buttons()

    def item(self, name):
        return next((i for i in self.items() if i.text()==name), None)

    def setValue(self, value):
        if not isinstance(value, (list, tuple)):
            value = (value,)

        for i in self.items():
            i.setChecked(i.text() in value)

    def value(self):
        values = [i.text() for i in self.items() if i.isChecked()]
        if self.buttonGroup.exclusive():
            return values[0] if values else []
        else:
            return values

    def setItems(self, items):
        self.blockSignals(True)
        for i in items:
            self.addItem(text=i)

        self.blockSignals(False)
        
        self.valueChanged.emit( self.value() )
        self.itemsChanged.emit(items)

    def addItem(self, icon=None, text=''):
        if text in [i.text() for i in self.items() ]:
            return 

        enumButton = EnumButton(self, text=text, icon=icon)
        self.buttonGroup.addButton(enumButton)

        if not self.value() or not self.buttonGroup.exclusive():
            enumButton.setChecked(True)

        self.itemsChanged.emit(self.items())

    def clear(self):
        for b in self.items():
            self.buttonGroup.removeButton(b)
            b.deleteLater()
        #self.buttonGroup.clear()

        #print('CLEAR', self, self.items())



class NumberSpinbox(HWidget):
    def __init__(self, parent=None, value=0, name='', min=None, max=None, decimals=2,
    step=0.01, reset=False, hard=False, connect=None, tag=None, **kargs):
        kargs = {'Rounded': 'normal', **kargs}

        super().__init__(parent, name=name, sizePolicy=('Expanding', 'Fixed'))

        #self.setAttribute(Qt.WA_StyledBackground, True)

        self.hover = False
        self.reset = reset
        self.defaultValue = value
        self.min = min
        self.max = max
        self.hard = hard

        if decimals == 0:
            min = -2147483648 if self.min is None else self.min
            max = 2147483647 if self.max is None else self.max
            self.spinbox = SpinBox(parent=self, value=value, min=min, max=max, step=step, 
            LPadding='small', RPadding='very_small', sizePolicy=('Expanding', 'Expanding'))
        else:
            min = -float('inf') if self.min is None else self.min
            max = float('inf') if self.max is None else self.max
            self.spinbox = DoubleSpinBox(parent=self, value=value, min=min, max=max, step=step,
            decimals=decimals, LPadding='small', RPadding='very_small', sizePolicy=('Expanding', 'Expanding'))



        self.spinbox.setFocusPolicy( Qt.StrongFocus )
        #self.spinbox.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.frame = HLayout(self.spinbox, margin=4)

 
        self.resetButton = WidgetButton(self.spinbox, icon='reset', Width=14,
        connect=lambda: self.spinbox.setValue(self.defaultValue))
        self.resetButton.setFocusPolicy(Qt.NoFocus)
        self.resetButton.hide()

        self.spinbox.addStretch()

        self.buttonFrame = HWidget(self.spinbox, spacing=1)

        self.leftButton = WidgetButton(self.buttonFrame, icon='arrow_down', Width=16,
        connect=lambda: self.spinbox.setValue(self.value()-self.spinbox.singleStep()) )
        self.leftButton.setFocusPolicy(Qt.NoFocus)
        self.leftButton.hide()

        self.rightButton = WidgetButton(self.buttonFrame, icon='arrow_up', Width=16, 
        connect=lambda: self.spinbox.setValue(self.value()+self.spinbox.singleStep()) )
        self.rightButton.setFocusPolicy(Qt.NoFocus)
        self.rightButton.hide()

        #Frame(self.spinbox, tag='very_small')
        #Frame(self.spinbox, tag='tiny')

        if reset:
            self.spinbox.valueChanged.connect(self.setResetBtn)
        if connect:
            self.spinbox.valueChanged.connect(connect)


    def enterEvent(self, e):
        super().enterEvent(e)
        self.hover = True
        self.setResetBtn()
        self.rightButton.show()
        self.leftButton.show()

    def leaveEvent(self, e):
        super().leaveEvent(e)
        self.hover = False
        self.setResetBtn()
        self.rightButton.hide()
        self.leftButton.hide()

    def setResetBtn(self):
        self.resetButton.setVisible(self.reset and self.value() != self.defaultValue)

    def value(self):
        return self.spinbox.value()

    def setValue(self, value):
        self.spinbox.setValue(value)

    def step(self):
        return self.spinbox.singleStep()

    #def checkValue(self, value):
    #    val = [i for i in range(min, max, step)]

class IntSpinbox(NumberSpinbox):
    valueChanged = Signal(int)

    def __init__(self, parent=None, value=0,  min=None, max=None, tag=None,
    step=1, connect=None, **kargs):
        super().__init__(parent=parent, value=value, min=min, max=max,
        decimals=0, step=step, connect=connect, tag=tag, **kargs)

        self.spinbox.valueChanged.connect(self.onValueChanged)

    def onValueChanged(self, value):
        self.spinbox.blockSignals(True)
        if value == self.value():
            return

        value = self.value()
        print('value: ', value)

        self.spinbox.setValue(value)
        self.spinbox.blockSignals(False)

        self.valueChanged.emit(value)

    def checkValue(self):
        return round((self.value() / self.step())) + (self.defaultValue / self.step())

    def value(self):
        if self.hard:
            return self.checkValue()
        else:
            return self.spinbox.value()


class FloatSpinbox(NumberSpinbox):
    valueChanged = Signal(float)

    def __init__(self, parent=None, value=0, min=None, max=None, tag=None,
    decimals=2, connect=None, **kargs):

        super().__init__(parent=parent, value=value, min=min, max=max,
        decimals=decimals, connect=connect, tag=tag, **kargs)

        self.spinbox.valueChanged.connect(self.valueChanged)


class PathLineEdit(HWidget):
    pathChanged = Signal(str)
    def __init__(self, parent=None, path='', name='', nameVisible=True,
    fileMode='ExistingFile', tag=None, **kargs):
        super().__init__(parent, name=name, nameVisible=nameVisible, tag=tag, **kargs)

        self.label = Label(self)
        self.lineEdit = LineEdit(self, RPadding=16)

        HLayout(self.lineEdit, margin=4)

        self.lineEdit.addStretch()

        self.fileDialog = QFileDialog(self)
        self.fileDialog.setFileMode(getattr(QFileDialog, fileMode) )

        self.openFileButton = WidgetButton(self.lineEdit, icon='folder')
        self.openFileButton.setFocusPolicy(Qt.NoFocus)
        #self.open_file_dlg
        self.openFileButton.clicked.connect(lambda: self.fileDialog.exec() )
        self.fileDialog.fileSelected.connect(lambda x: self.lineEdit.setText(x) )

        self.lineEdit.textChanged.connect(lambda x: self.pathChanged.emit(x) )

        #print('PathLine Edit', 'name', name, 'self._name', self._name)
        self.setName(name)
        self.setNameVisible(self.nameVisible)

    def name(self):
        return self.label.text()

    def setName(self, name):
        self.label.setText(name)
    
    def setNameVisible(self, visible):
        self.label.setVisible(bool(visible))

    def path(self):
        return self.lineEdit.text()

    def setPath(self, value):
        self.lineEdit.setText(value)


class ColorButton(HWidget):
    colorChanged = Signal(QColor)

    def __init__(self, parent=None, color=QColor(), name='',
     tag=None, **kargs):
        super().__init__(parent, name=name, tag=tag, **kargs)

        self._defaultColor = color
        self._color = self._defaultColor

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.label = Label(self, name)
        self.colorButton = PushButton(self, tag='ColorPropBtn', connect=self.pickColor)

        self.setName(name)
        self.setNameVisible(self.nameVisible)

    def setName(self, name):
        self.label.setText(name)
        self.label.setVisible(bool(name))

    def setNameVisible(self, visible):
        self.label.setVisible(bool(visible))

    def name(self):
        return self.label().text()

    def pickColor(self):
        color = QColorDialog.getColor(initial=self._color, options=QColorDialog.ShowAlphaChannel)
        self.setColor(color)

    def defaultColor(self):
        return self._defaultColor

    def color(self):
        return self._color

    def setColor(self, color):
        self._color = color
        self.setStyleSheet("QPushButton { background-color: %s}" % color.name())
        self.colorChanged.emit(color)







class UIList(HWidget):
    itemsChanged = Signal(list)
    def __init__(self, parent=None, name='', nameVisible=True, type='String', reset=False,
    items=[], tag=None, **kargs):
        super().__init__(parent, name=name, nameVisible=nameVisible, 
        sizePolicy=('Expanding', 'Fixed'), **kargs)

        self.type = type

        self.default_items = [str(i) for i in items]
        self.reset = reset

        self.label = Label(self)

        self.frame = VWidget(self, align='Top')

        self.headerWidget = HWidget(self.frame, size=(-1, 24), Background='dark')
        
        #VLayout(self.listWidget, align='Top')

        self.headerWidget.addStretch()
        self.removeButton = WidgetButton(self.headerWidget, icon='minus', size=(18,18), iconSize=15,
        connect=lambda : self.remove())

        self.addButton = WidgetButton(self.headerWidget, icon='plus', size=(18,18), iconSize=16,
        connect=lambda : self.add())

        self.listWidget = VListWidget(self.frame, tag='UIList')
        #self.header = self.listWidget.addItem(widget=self.headerWidget)
        #self.header.setFlags(None)

        #self.listWidget.setResizeMode(QListView.Adjust)

        #self.listWidget.setAttribute(Qt.WA_NoSystemBackground)

        #self.frame = VWidget(self.listWidget)
        #self.listWidget.setLayout(VLayout(align='Bottom'))
        #self.frame = 
        #print('\n>>>>', self.listWidget.layout())
        #self.label = PushButton(self.listWidget.layout(), text= 'TOTO')
        #self.label = TextLabel(self.listWidget.layout(), text= 'TOTO')

        #self.listWidget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        '''
        #self.frame = HWidget(self.listWidget)
        buttonLayout = VLayout(self, align='Top')
        self.buttonsFrame = VWidget(buttonLayout, spacing=1, margin=2, tag='UIList', sizePolicy=('Fixed', 'Fixed'))
        #self.buttonsFrame.s
        
        self.addButton = WidgetButton(self.buttonsFrame, icon='plus', size=(18,18), iconSize=16,
        connect=lambda: self.add())

        self.removeButton = WidgetButton(self.buttonsFrame, icon='minus', size=(18,18), iconSize=16,
        connect=lambda: self.remove())

        '''

        '''
        self.resetButton = IconButton(self.buttonsFrame, tag='Reset', 
        connect=lambda: self.setItems(self.default_items), focus='NoFocus' )
        self.resetButton.hide()
        '''
        #self.buttonsFrame.addStretch()

        #self.setName(name)
        #self.setNameVisible(self.nameVisible)

        self.itemsChanged.connect(self.setResetButton )

        self.listWidget.model().rowsRemoved.connect(self.onItemChanged)
        self.listWidget.model().rowsInserted.connect(self.onItemChanged)
        self.listWidget.itemChanged.connect(self.onItemChanged)

        self.onItemChanged()

        self.setName(name)
        self.setNameVisible(self.nameVisible)

    def onItemChanged(self):
        items = self.items()
        self.itemsChanged.emit(self.items())

        self.listWidget.setFixedHeight(23*len(items)+2)

    def setResetButton(self):
        return
        visibility = self.reset and self.items() != self.default_items
        self.resetButton.setVisible(visibility)

    def name(self):
        return self.label.text()

    def setName(self, name=''):
        self.label.setText(name)
    
    def setNameVisible(self, visible):
        self.label.setVisible(bool(visible))

    def items(self):
        return [i.text() for i in self.listWidget.items()]

    def setItems(self, items):
        self.listWidget.clear()
        self.listWidget.blockSignals(True)
        for i in items:
            self.add(i)
        self.listWidget.blockSignals(False)

        self.itemsChanged.emit(self.items() )

    def add(self, text=''):
        text=str(text)
        #print('\nTEXT', text)
        i = self.listWidget.addItem(text, editable=True)
        i.setSizeHint(QSize(0, 23))
        self.listWidget.setCurrentItem(i)

    def remove(self, text=''):
        self.listWidget.takeItem(self.listWidget.currentRow())

    #def to_list(self):
    #    return [i.text() for i in self.list_widget.items()]