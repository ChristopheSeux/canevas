from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QProxyStyle, QStyle, QCommonStyle
from PySide2.QtCore import Signal, Qt, QObject, QSize

from .properties import PropertyGroup, EnumProperty
from .theme import Themes
from .stylesheet import StyleSheet

import sys
import os
from pathlib import Path


class Data :
    def __init__(self, app):
        self.app = app
        self.themes = Themes.from_dir(app.themes_dir)


class Settings:
    def __init__(self, context):

        self.app = context.app

        self.display = PropertyGroup(
            ui_scale=EnumProperty('100%', items=['100%', '125%', '150%', '175%'], name='UI Scale'),
            theme=EnumProperty('dark', items=['dark', 'light'], name='Theme')
        )

        self.display['ui_scale'].value_changed.connect(self.on_ui_scale_change)
        self.display['theme'].value_changed.connect(self.on_theme_change)

    def on_ui_scale_change(self, scale=None):
        self.app.signals.ui_scale_changed.emit(self.app.context.ui_scale)

    def on_theme_change(self, theme=None):
        self.app.signals.theme_changed.emit(theme)


class Context:
    def __init__(self, app):
        self.app = app
        self.settings = Settings(self)
        self.themes = Themes.from_dir(app.themes_dir)
        
    @property
    def theme(self):
        return self.app.data.themes[self.settings.display.theme]

    @property
    def ui_scale(self):
        scale_convert = {'100%': 1.0, '125%' :1.25, '150%' : 1.5, '175%' : 1.75}
        return scale_convert.get(self.settings.display.ui_scale)


class Signals(QObject) :
    ui_scale_changed = Signal(float)
    theme_changed = Signal(float)

    def __init__(self, app):
        super().__init__()

        self.app = app
        

'''
class AppProxyStyle(QProxyStyle):
    def pixelMetric(self, metric, option, widget):
        print(metric)

        app = QApplication.instance()

        if metric is QStyle.PM_LayoutLeftMargin:
            print(1)
            return widget.layout()._contentsMargins.left() * app.context.ui_scale
        elif metric is QStyle.PM_LayoutRightMargin:
            print(2)
            return widget.layout()._contentsMargins.right() * app.context.ui_scale
        elif metric is QStyle.PM_LayoutTopMargin:
            print(3)
            return widget.layout()._contentsMargins.top() * app.context.ui_scale
        elif metric is QStyle.PM_LayoutBottomMargin:
            print(4)
            return widget.layout()._contentsMargins.bottom() * app.context.ui_scale
        elif metric is QStyle.PM_LayoutHorizontalSpacing:
            print(5)
            return widget.layout()._spacing * app.context.ui_scale
        elif metric is QStyle.PM_LayoutVerticalSpacing:
            print(6)
            return widget.layout()._spacing * app.context.ui_scale * 5
        
        return app.context.ui_scale*int(metric)
'''

'''

    def drawComplexControl(self, control, option, painter, widget):
        print('drawComplexControl')
        return super().drawComplexControl(control, option, painter, widget)

    def drawControl(self, element, option, painter, widget):
        print('drawControl')
        return super().drawControl(element, option, painter, widget)

    def drawPrimitive(self, element, option, painter, widget):
        print('drawPrimitive')
        return super().drawPrimitive(element, option, painter, widget)

    def sizeFromContents(self, type, option, size, widget):
        print('sizeFromContents')
        return super().sizeFromContents(type, option, size+QSize(100, 100), widget)

    def subControlRect(self, cc, option, sc, widget):
        print('subControlRect', widget)
        return super().subControlRect(cc, option, sc, widget)

    def subElementRect(self, element, option, widget):
        print('subElementRect', widget)
        return super().subElementRect( element, option, widget)
'''

class QtApplication(QApplication):
    def __init__(self, argv=sys.argv):

        os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
        os.environ["QT_SCALE_FACTOR"] = "1"

        QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)
        QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)
        QApplication.setStyle('Fusion')

        super().__init__(argv)


class Application(QtApplication):
    def __init__(self, argv=sys.argv, icons_dir=None, themes_dir=None, css=None):
        super().__init__(argv)

        self.MODULE_DIR = Path(__file__).parents[1]

        module_name = self.MODULE_DIR.stem.upper()
        os.environ[module_name+'_DIR'] = str(self.MODULE_DIR)

        if icons_dir is None:
            self.icons_dir = self.MODULE_DIR/'resources'/'icons'

        if themes_dir is None:
            self.themes_dir = self.MODULE_DIR/'resources'/'themes'

        if css is None:
            self.css = self.MODULE_DIR/'resources'/'stylesheet.css'

        print('themes_dir', self.themes_dir)

        self.signals = Signals(self)
        self.data = Data(self)
        self.context = Context(self)

        sys.modules.update( {
            'canevas.signals': self.signals,
            'canevas.app': self,
            'canevas.context': self.context,
            'canevas.data': self.data,
        })

        properties = self.context.theme.properties

        self.context.themes['dark'].apply()

        #print(self.context.theme.colors.background_color)

        