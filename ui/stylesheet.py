
from PySide2.QtWidgets import QApplication
from PySide2.QtGui import QColor
from pathlib import Path

import yaml
from os.path import exists
from functools import partial
import re
import os


class StyleProperties:
    def __init__(self, widget, data={}):
        super().__init__()

        self._widget = widget
        self._data = []

        self._block_update = True
        self.update(data)
        self._block_update = False
        
    @property
    def block_update(self):
        return self._block_update

    @block_update.setter
    def block_update(self, value=True):
        self._block_update = value

    @property
    def widget(self):
        return self._widget

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    def updateWidget(self):
        if not self.block_update:
            self._widget.style().unpolish(self._widget)
            self._widget.style().polish(self._widget)
            self._widget.update()

    def update(self, data={}):
        _block_update = self.block_update

        self.block_update = True
        for k, v in data.items():
            self[k] = v    
        
        self.block_update = _block_update

        self.updateWidget()

    def clear(self):
        self._data = []

    def to_dict(self):
        return {k: v for k,v in self.data}

    def items(self):
        return self.to_dict().items()

    def keys(self):
        return self.to_dict().keys()

    def __setitem__(self, key, value):
        self._data.append((key, value))

        if isinstance(value, (float, int)):
            value = 'px_%s'%value

        if key == 'Margin':
            for k in ('LMargin', 'LMargin', 'TMargin', 'BMargin'):
                self._widget.setProperty(k, value)
        elif key == 'HMargin':
            for k in ('LMargin', 'RMargin'):
                self._widget.setProperty(k, value)
        if key == 'VMargin':
            for k in ('TMargin', 'BMargin'):
                self._widget.setProperty(k, value)
        if key == 'Padding':
            for k in ('LPadding', 'RPadding', 'TPadding', 'BPadding'):
                self._widget.setProperty(k, value)
        if key == 'HPadding':
            for k in ('LPadding', 'RPadding'):
                self._widget.setProperty(k, value)
        if key == 'VPadding':
            for k in ('TPadding', 'BPadding'):
                self._widget.setProperty(k, value)
        if key == 'Size':
            for k in ('Width', 'Height'):
                self._widget.setProperty(k, value)
        else:
            self._widget.setProperty(key, value)

        self.updateWidget()

    def __getitem__(self, key):
        if isinstance(key, int):
            return self._data[key][1]
        elif isinstance(key, str):
            return self.to_dict()[key]

    def __delitem__(self, key):
        if isinstance(key, int):
            self._data.pop(key)
        elif isinstance(key, str):
            i = next(i for i, k in enumerate(self._data) if k[0] == key)
            self._data.pop(i)

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return (i[1] for i in self._data)

    def __repr__(self):
        text = '\nStyleProperties(\n'
        for k, v in self._data:
            text+= f"    {k}: {v}\n"
        text+= ')\n'

        return text

    def get(self, name, other=None):
        if name in self.keys():
            return self[name]
        
        return other


class StyleSheet:
    def __init__(self, theme, stylesheet, constants={}, properties={}):

        self.theme = theme
        self.stylesheet = stylesheet

        self.constants = constants
        self.properties = properties

        self.field_reg = re.compile('\[(.*?)\]')
        self.color_reg = re.compile('rgba\(([0-9.]+), ([0-9.]+), ([0-9.]+), ([0-9.]+)\)')

        for k, p in properties.items():
            p.value_changed.connect(self.set)

        #app = QApplication.instance()
        #if hasattr(app, 'signals'):
        #    app.signals.ui_scale_changed.connect(self.set)

    @classmethod
    def from_file(cls, theme, file, constants={}, properties={}):
        file = Path(file)
        if not file.exists():
            raise Exception(f'The stylesheet {file} does not exist')
        
        return cls(theme, file.read_text(), constants, properties)

    def color_to_string(self, color):

        #print('\n//////', color, type(color))
        return f'rgba({color.red()}, {color.green()}, {color.blue()}, {color.alpha()})'

    def string_to_color(self, color):

        #print('\n//////', color, type(color))
        return QColor(*[float(i) for i in self.color_reg.findall(color)[0]])

    def brightness(self, color, ratio=0.5, alpha=255):
        c = color
        if not isinstance(color, QColor):
            c = self.string_to_color(color)

        c = QColor(c)
        c.setAlpha(alpha)

        if ratio > 0 :
            c = c.lighter(100 + ratio * 100)
        else:
            c = c.darker(100 - ratio * 200)

        if isinstance(color, QColor):
            return c
        else:
            return self.color_to_string(c)

    def set(self):
        print('\nUpdate Style', self.theme.name)

        css = self.stylesheet

        attrs = self.constants.copy()

        #print(attrs)

        attrs['brightness'] = self.brightness
        attrs['ui_scale'] = 1 ### FOR NOW
        attrs['colors'] = self.theme.colors
        attrs['borders'] = self.theme.borders
        attrs['icon_dir'] = self.theme.icon_dir.value
        #attrs['ICON_DIR'] = Path(os.getenv('ICON_DIR'))

            #print('\nTheme', app.context.theme.name)
        #attrs['icons_dir'] = QApplication.instance().icons_dir

        matchs = [(e.groups()[0], e.group()) for e in self.field_reg.finditer(css)]

        for result, match in matchs:
            if '=' in result: continue
            v = eval(result, attrs)

            if isinstance(v, QColor):
                v = self.color_to_string(v)
            elif isinstance(v, Path):
                v = f'url({v.as_posix()})'

            css = css.replace(match, str(v),1)

        #print(css)
        #/home/christophe/Documents/Programmation/doliprane-2.1.0/gadget/resources/icons/close_32.png
        app = QApplication.instance()
        app.setStyleSheet(css)
