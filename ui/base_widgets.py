
from PySide2.QtWidgets import QApplication, QSizePolicy, \
QLayout, QBoxLayout, QStackedLayout, QFormLayout, QMenu, QWidgetAction, \
QWidget, QTabWidget, QFrame, QLabel, QAbstractSpinBox, QSpinBox, QDoubleSpinBox, \
QListWidget, QListWidgetItem, QScrollArea, QDialog, QCheckBox, QComboBox, QPushButton, \
QLineEdit, QPlainTextEdit, QSplitter, QListView, QColorDialog, \
QAbstractItemView, QProgressBar, QButtonGroup, QStackedWidget, QTableWidget,\
QTableWidgetItem, QGridLayout, QGroupBox, QAbstractScrollArea, QTextEdit



from PySide2.QtWidgets import QGraphicsDropShadowEffect

from PySide2.QtCore import Qt, Signal, QPointF, QLineF, QTime, QMargins, QSize, QEvent
from PySide2.QtGui import QPixmap, QImage, QIcon, QPainter, QFontMetrics, QTextCursor, \
QColor, QPen, QRegion

from pathlib import Path
import re

from .stylesheet import StyleProperties


class AbstractWidget:
    def __init__(self, parent=None, name='', nameVisible=True, cursor=None, 
    focus=None, sizePolicy=None, align=None, size=None, toolTip='', tag=None, **kargs):

        app = QApplication.instance()
        
        self.defaultSize = size or (None, None)
        self.nameVisible = nameVisible

        if focus:
            self.setFocusPolicy(getattr(Qt, focus))

        hp = self.sizePolicy().horizontalPolicy()
        vp = self.sizePolicy().verticalPolicy()

        if size:
            width, height = size
            if width ==-1:
                hp = QSizePolicy.Expanding
            elif width is not None:
                self.setFixedWidth(width)
                hp = QSizePolicy.Fixed
            if height ==-1:
                vp = QSizePolicy.Expanding
            elif height is not None:
                self.setFixedHeight(height)
                vp = QSizePolicy.Fixed

        if sizePolicy:
            if isinstance(sizePolicy, (tuple, list)):
                hp, vp = (getattr(QSizePolicy, p) for p in sizePolicy)
            else:
                hp = getattr(QSizePolicy, sizePolicy)

        self.setSizePolicy(hp, vp)

        if align:
            if isinstance(align, (tuple, list) ):
                align = getattr(Qt, 'Align'+align[0]) | getattr(Qt, 'Align'+align[1])
            else:
                align = getattr(Qt, 'Align'+align)

            self.setAlignment(align)

        if tag:
            self.setObjectName(tag)
        if cursor:
            self.setCursor(getattr(Qt, cursor))
        if toolTip:
            self.setToolTip(toolTip)


        self.styleProperties = StyleProperties(self, kargs)

        layout = parent

        if hasattr(parent,'layout') and not hasattr(parent, 'addWidget'):
            layout = parent.layout()

        if not layout: return

        self.nameVisible = True if name else False
        if hasattr(layout, 'addRow'):
            self.nameVisible = False
            layout.addRow(name, self)
        else:
            layout.addWidget(self)

    def addStretch(self):
        layout = self.layout()
        if layout:
            layout.addStretch()

    def clear(self):
        layout = self.layout()
        if layout:
            layout.clear()


class AbstractLayout:
    def __init__(self, parent=None, spacing=0, margin=0, align=None):

        app = QApplication.instance()

        self.setSpacing(spacing)

        if isinstance(margin, (float, int)):
            margin = [margin]*4
        elif isinstance(margin, (list, tuple)) and len(margin)==2:
            margin = margin*2

        self.setContentsMargins(*margin)

        if align:
            if isinstance(align, (tuple, list) ):
                align = getattr(Qt, 'Align'+align[0]) | getattr(Qt, 'Align'+align[1])
            else:
                align = getattr(Qt, 'Align'+align)

            self.setAlignment(align)

        if parent and parent.layout():
            parent.layout().addLayout(self)
        elif parent:
            parent.setLayout(self)

        

        #if hasattr(app, 'signals'):
        #    app.signals.ui_scale_changed.connect(self.updateSpacing)
        #    app.signals.ui_scale_changed.connect(self.updateMargins)

    '''
    def spacing(self):
        return self._spacing

    def setSpacing(self, spacing):
        self._spacing = spacing

    def contentsMargins(self):
        return self._contentsMargins

    def setContentsMargins(self, *margins):
        self._contentsMargins = QMargins(*margins)
        #self.updateMargins()
    '''
    #def get_ui_scale(self):
    #    app = QApplication.instance()
    #    return app.context.ui_scale if hasattr(app, 'signals') else 1.0

    #def spacing(self):
    #    return self._spacing

    #def setSpacing(self, spacing):
    #    self._spacing = spacing
    #    self.updateSpacing()

    #def updateSpacing(self, scale=None):
    #    scale = scale or self.get_ui_scale()
    #    super().setSpacing(self._spacing*scale)

    #def contentsMargins(self):
    #    return self._spacing

    #def setContentsMargins(self, *margins):
    #    self._margins = margins
    #    self.updateMargins()

    #def updateMargins(self, scale=None):
    #    scale = scale or self.get_ui_scale()
    #    super().setContentsMargins(*[m*scale for m in self._margins])



    def clearLayout(self, layout):
        items = [self.takeAt(i) for i in range(layout.count())[::-1]]
        for i in items:
            if not i: 
                continue
            if i.layout():
                self.clearLayout(i.layout())

            w = i.widget()
            if w:
                if hasattr(w, 'clear'):
                    w.clear()
                if w.layout():
                    self.clearLayout(w.layout())
                w.blockSignals(True)
                w.deleteLater()
                w.setParent(None)
                #widgets.append(w)
            del i     


    def clear(self):
        #print('CLEAR', self)
        self.clearLayout(self)
        '''
        for w in widgets:
            w.deleteLater()
            w.setParent(None)
            del w
            '''
        
        #del widgets
        


class Layout(AbstractLayout, QBoxLayout):
    countChanged = Signal()

    def __init__(self, parent=None, orientation='LeftToRight', spacing=0,
    margin=0, align=None, collapse=False):
        QBoxLayout.__init__(self, getattr(QBoxLayout, orientation))
        AbstractLayout.__init__(self, parent, spacing, 
        margin, align)

        self.collapse = collapse
        #self.installEventFilter(self)
    #def geometry(self):
    #    print('update', self)
    #def addItem(self, item):
    #    print(item)


    def addWidget(self, widget):
        super().addWidget(widget)
        self.countChanged.emit()

    def removeWidget(self, widget):
        super().removeWidget(widget)
        self.countChanged.emit()
    '''
    def takeAt(self, i)

    def setGeometry(self, r):
        print(r)
        #print(self, e, e.type())
        super().setGeometry(r)
    '''

class HLayout(Layout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, collapse=False):
        super().__init__(parent, orientation='LeftToRight', spacing=spacing, 
        margin=margin, align=align, collapse=collapse)

        self.countChanged.connect(self.collapseWidgets)

    def row(self, *widgets, align=None):
        layout = HLayout(self, align=align, spacing=5)
        for w in widgets:
            if isinstance(w, str):
                w = TextLabel(text=w, align='Right')
            layout.addWidget(w)

    def collapseWidgets(self):
        if not self.collapse:
            return 

        for index in range(self.count()):
            item = self.itemAt(index)

            widget = item.widget()

            if not widget:
                return

            data = {}
            if index == 0: 
                data['RBorderRadius'] = 0
            elif 0 < index <= self.count():
                data['LBorderWidth'] = 0
                data['LBorderRadius'] = 0
                data['RBorderRadius'] = 0
            else:
                data['LBorderRadius'] = 0

            widget.styleProperties.update(data)

class VLayout(Layout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, collapse=False):
        super().__init__(parent, orientation='TopToBottom', spacing=spacing, 
        margin=margin, align=align, collapse=collapse)

        self.countChanged.connect(self.collapseWidgets)

    def collapseWidgets(self):
        if not self.collapse:
            return 

        for index in range(self.count()):
            item = self.itemAt(index)

            widget = item.widget()

            if not widget:
                return

            data = {}
            if index == 0: 
                data['BBorderRadius'] = 0
            elif 0 < index <= self.count():
                data['BBorderWidth'] = 0
                data['BBorderRadius'] = 0
                data['TBorderRadius'] = 0
            else:
                data['TBorderRadius'] = 0

            item.styleProperties.update(data)

class StackedLayout(AbstractLayout, QStackedLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None,
    mode='StackOne'):

        QStackedLayout.__init__(self)
        AbstractLayout.__init__(self, parent, spacing, margin, align)

        self.setStackingMode( getattr(QStackedLayout, mode) )

    '''
    def addWidget(self, widget):
        spacing = self.property('Spacing')
        if spacing and self.count():
            super().addWidget(Frame(tag=spacing))
        super().addWidget(widget)
    '''


    '''
    def sizeHint(self):
        if self.currentWidget():
            return self.currentWidget().sizeHint()
        else:
            return super().sizeHint()
    def minimumSizeHint(self):
        if self.currentWidget():
            return self.currentWidget().minimumSizeHint()
        else:
            return super().minimumSizeHint()
    '''

class FormLayout(AbstractLayout, QFormLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, labelAlign='Right'):
        QFormLayout.__init__(self)
        AbstractLayout.__init__(self, parent, spacing, margin, align)

        #self.setAlignment(Qt.AlignTop )
        #self.setLabelAlignment(getattr(Qt,'Align'+labelAlign) )
        #self.setFormAlignment(Qt.AlignTop )
        self.setLabelAlignment( getattr(Qt,'Align'+labelAlign)|Qt.AlignVCenter)



    def addRow(self, label='', widget=None):
        textLabel = TextLabel(text=label)
        #textLabel.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        super().addRow(textLabel, widget)

        '''
    def addRow(self, text='', field=None):
        label = TextLabel(text=text, Hmargin='normal')
        spacing = self.property('Spacing')
        if spacing and self.count():
            super().addRow( Frame(tag=spacing) )

        super().addRow(label, field)
        '''

class GridLayout(AbstractLayout, QGridLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None):
        QGridLayout.__init__(self)
        AbstractLayout.__init__(self, parent, spacing, margin, align)


class Widget(AbstractWidget, QWidget):
    def __init__(self, parent=None, title='', icon='', tag=None, **kargs):
        QWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag, **kargs)

        if title:
            self.setWindowTitle(title)
        if icon:
            self.setWindowIcon(Icon(icon))

        '''
    def paintEvent(self, p):
        opt = QStyleOption()
        opt.init(self)
        p = QPainter(self)
        self.style().drawPrimitive(QStyle.PE_Widget, opt, p, self)
        '''


class TabWidget(AbstractWidget, QTabWidget):
    tabChanged = Signal(str)
    def __init__(self, parent=None, tabs=[], title='', focus='StrongFocus',
    tag=None, **kargs):
        QTabWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        for t in tabs:
            self.addTab(tab=t)

        self.setWindowTitle(title)

        self.currentChanged.connect(lambda x: self.tabChanged.emit(self.currentTab()) )

    def currentTab(self):
        return self.tabText(self.currentIndex() )

    def setCurrentTab(self, tab):
        #print('Set Current Tab', tab, self.widget(tab))
        self.setCurrentWidget(self.widget(tab))

    def addTab(self, widget=None, tab='', icon=None):
        super().addTab(widget or Widget(), tab)

    def setTabWidget(self, tab, widget):
        current = self.currentIndex()
        if self.widget(tab):
            self.widget(tab).setParent(None)

        self.addTab(widget, tab)

        self.setCurrentIndex(current)

    def widget(self, tab):
        if isinstance(tab, str):
            tabs = self.tabs()
            if tab in tabs:
                return super().widget( self.tabs().index(tab) )
        else:
            return super().widget(tab)

    def widgets(self):
        return [self.widget(i) for i in range(self.count())]

    def tabs(self):
        return [self.tabText(i) for i in range(self.count())]


class Frame(AbstractWidget, QFrame):
    def __init__(self, parent=None, title='', icon='', tag=None, **kargs):

        QFrame.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        if title:
            self.setWindowTitle(title)
        if icon:
            self.setWindowIcon(Icon(icon))

    def addRow(self, label='', widget=None):
        row = HLayout(self, spacing=10)
        TextLabel(row, text=label, align='Right')
        if widget:
            sizePolicy = widget.sizePolicy()
            sizePolicy.setHorizontalPolicy(QSizePolicy.Expanding)
            widget.setSizePolicy(sizePolicy)

            row.addWidget(widget)

        #self.layout().addRow(label , widget)


class GroupBox(AbstractWidget, QGroupBox):
    def __init__(self, parent=None, title='', tag=None, **kargs):

        QGroupBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        if title:
            self.setTitle(title)


class Separator(Frame):
    def __init__(self, parent=None, orientation='Horizontal', tag=None, **kargs):
        sizePolicy = None

        if orientation == 'Horizontal':
            kargs['Width'] = 1
            sizePolicy=('Fixed', 'Expanding')

        elif orientation == 'Vertical':
            kargs['Height'] = 1
            sizePolicy=('Expanding', 'Fixed')

        super().__init__(parent=parent, tag=tag, sizePolicy=sizePolicy, **kargs)

        self.setFrameShape(QFrame.HLine)
        self.setFrameShadow(QFrame.Sunken)


class HSeparator(Separator):
    def __init__(self, parent=None, text='', tag=None, **kargs):
        super().__init__(parent=parent, tag=tag, orientation='Horizontal', **kargs)


class VSeparator(Frame):
    def __init__(self, parent=None, text='', tag=None, **kargs):
        super().__init__(parent=parent, tag=tag, orientation='Vertical', **kargs)


class HWidget(Frame):
    def __init__(self, parent=None, title='', icon='', spacing=0, margin=0,
    align=None, tag=None, collapse=False, **kargs):
        super().__init__(parent, title=title, icon=icon, tag=tag, **kargs)

        HLayout(self, spacing=spacing, margin=margin, align=align, collapse=collapse)

    '''
    def paintEvent(self, QPaintEvent):
        
        super().paintEvent(QPaintEvent)

        myQPainter = QPainter(self)
        pen = QPen(QColor(0,0,0,80), 1, Qt.SolidLine)

        rect = self.layout().contentsRect()

        x = 0
        for i in range(self.layout().count()-1):
            item = self.layout().itemAt(i)

            x += item.geometry().width()+self.layout().spacing()/2
        
            myQPainter.setPen(pen)
            myQPainter.drawLine(x, rect.y()+4, x, rect.y()+rect.height()-5)
    '''
 


class VWidget(Frame):
    def __init__(self, parent=None, title='', icon='', spacing=0, margin=0,
    align=None, tag=None, collapse=False, **kargs):
        super().__init__(parent, title=title, icon=icon, tag=tag, **kargs)

        VLayout(self, spacing=spacing, margin=margin, align=align, collapse=collapse)


class FWidget(Frame):
    def __init__(self, parent=None, title='', icon='', spacing=0, margin=0,
    align=None, tag=None, horizontalSpacing=6, **kargs):
        super().__init__(parent, title=title, icon=icon, tag=tag, **kargs)

        f = FormLayout(self, align=align, spacing=spacing, margin=margin)
        f.setHorizontalSpacing(horizontalSpacing)

    def addRow(self, label='', widget=None):
        self.layout().addRow(label , widget)


class StackedWidget(AbstractWidget, QStackedWidget):
    def __init__(self, parent=None, mode='StackOne', spacing=0, margin=0,
    align=None, tag=None, **kargs):
        QStackedWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.setLayout ( StackedLayout(spacing=spacing, margin=margin, mode='StackOne') )
        #self.layout().setStackingMode( getattr(QStackedLayout, mode) )
        #print( '\n>>>>>', self.layout() )

    def clear(self):
        for i in range(self.count()):
            widget = self.widget(i)
            self.removeWidget(widget)
            if widget:
                widget.deleteLater()

    '''
    def sizeHint(self):
        w = self.currentWidget()
        return w.sizeHint() if w else super().sizeHint()
    
    def minimumSizeHint(self):
        w = self.currentWidget()
        return w.minimumSizeHint() if w else super().minimumSizeHint()
    '''
    def widgets(self):
        return [ self.widget(i) for i in range( self.count() ) ]


class GridWidget(Frame):
    def __init__(self, parent=None, title='', icon='', spacing=0, margin=0,
    align=None, tag=None, **kargs):
        super().__init__(parent, title=title, icon=icon, tag=tag, **kargs)

        GridLayout(self, spacing=spacing, margin=margin, align=align)


class Icon(QIcon):
    def __init__(self, icon=None):
        super().__init__()

        if not icon:
            return

        app = QApplication.instance()

        self.namingReg = re.compile(r'([_\w]+)_([\d]+)')
        #self.shadowColor = shadowColor

        self.iconPath = None
        if isinstance(icon, (str, Path)):
            if any([i in str(icon) for i in ('\\', '/')]): # it's a path
                self.iconPath = Path(icon)
            elif hasattr(app, 'theme'):
                self.iconPath = app.theme.icon_dir.value/icon
            else:
                return

            if self.iconPath.exists():
                if self.iconPath.is_dir():
                    for i in self.iconPath.glob('*.*'):
                        #print(i)
                        res = self.namingReg.findall(str(i.stem))
                        if res:
                            name, width = res[0]
                            width = int(width)
                            self.addFile(i.as_posix(), QSize(int(width/2), int(width/2)))
                            #pixmap = QPixmap(i.as_posix()).scaled(*size, transformMode=Qt.SmoothTransformation)
                            #self.addFile(i.as_posix(), QSize(int(width/2), int(width/2)))
                        else:
                            self.addFile(i.as_posix())
                    # self.addPixmap(pixmap)
                else:
                    self.addFile(self.iconPath.as_posix())
            else:
                return

                #self.addFile(self.iconPath.as_posix())

            #self.defaultPixmap = QPixmap(self.iconPath.as_posix())

        elif isinstance(icon, QPixmap):
            #self.defaultPixmap = icon
            self.addPixmap(icon)
        #print(self.iconPath)
        #print(app.context.theme.icons[icon])
        #print('/////')
        #print(icon, self.iconPath, type(self.iconPath))


        #if self.defaultPixmap.isNull():
        #    return
        
        #self.defaultSize = self.defaultPixmap.size()
        #self.shadowPixmap = self.getShadow()

        #self.addPixmap(self.getNormalPixmap(shadowColor), QIcon.Normal)
        #self.addPixmap(self.getActivePixmap(shadowColor), QIcon.Active)
        #self.addPixmap(self.getDisabledPixmap(), QIcon.Disabled)

    def getShadowPixmap(self, color):
        shadowPixmap = QPixmap(self.defaultSize)
        shadowPixmap.fill(Qt.transparent)

        painter = QPainter(shadowPixmap)
        painter.setOpacity(0.5)
        painter.drawPixmap(-1, -1, self.defaultPixmap)
        painter.drawPixmap(1, 1, self.defaultPixmap)
        painter.drawPixmap(2, 2, self.defaultPixmap)
        painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
        painter.fillRect(shadowPixmap.rect(), color)
        painter.end()

        return shadowPixmap
    
    def getOverlayPixmap(self):
        color = QColor(255, 255, 255)

        overlayPixmap = QPixmap(self.defaultSize)
        overlayPixmap.fill(Qt.transparent)
        painter = QPainter(overlayPixmap)
        painter.drawPixmap(0, 0, self.defaultPixmap)
        painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
        painter.fillRect(overlayPixmap.rect(), color)
        painter.end()

        return overlayPixmap

    def getNormalPixmap(self, shadowColor=None):
        normalPixmap = QPixmap(self.defaultSize)
        normalPixmap.fill(Qt.transparent)

        painter = QPainter(normalPixmap)
        if shadowColor:
            painter.drawPixmap(0, 0, self.getShadowPixmap(shadowColor))
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawPixmap(0, 0, self.defaultPixmap)
        painter.end()

        return normalPixmap

    def getActivePixmap(self, shadowColor=None, intensity=0.25):
        highlightPixmap = QPixmap(self.defaultSize)
        highlightPixmap.fill(Qt.transparent)

        overlayPixmap = self.getOverlayPixmap()

        painter = QPainter(highlightPixmap)
        if shadowColor:
            painter.drawPixmap(0, 0, self.getShadowPixmap(shadowColor))
        painter.setOpacity(1)
        painter.drawPixmap(0, 0, self.defaultPixmap)
        painter.setCompositionMode(QPainter.CompositionMode_ColorDodge)
        painter.setOpacity(intensity)
        painter.drawPixmap(0, 0, overlayPixmap)
        painter.end()

        return highlightPixmap

    def getDisabledPixmap(self):
        disabledPixmap = QPixmap(self.defaultPixmap)
        grayImage = self.defaultPixmap.toImage().convertToFormat(QImage.Format_Grayscale8)
        grayPixmap = QPixmap.fromImage(grayImage)

        painter = QPainter(disabledPixmap)
        painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
        painter.drawPixmap(0, 0, grayPixmap)
        painter.end()
        
        return disabledPixmap


class ScrollArea(AbstractWidget, QScrollArea):
    def __init__(self, parent=None, widget=None, tag=None, **kargs):
        QScrollArea.__init__(self)
        AbstractWidget.__init__(self, parent, tag, **kargs)

        self.setWidget(widget)
        self.setWidgetResizable(True)


class VScrollArea(ScrollArea):
    def __init__(self, parent=None, widget=None, tag=None, **kargs):
        super().__init__(parent, widget, tag, **kargs)

        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setWidgetResizable(True)


class WidgetLabel(HWidget):
    def __init__(self, parent=None, text='', align='Right', tag=None, **kargs):
        kargs= {'Height': 24, 'HMargin': 8, **kargs}

        super().__init__(parent, tag=tag, **kargs)

        self._text = text
        self.text = text
        self.align = align

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)


    def paintEvent(self, event):
        p = QPainter(self)
        p.setFont(self.font())

        #p.setPen(QColor(255,255,255,255))

        rect = self.rect().marginsRemoved(self.contentsMargins())

        p.drawText(rect, getattr(Qt, 'Align'+self.align)|Qt.AlignVCenter,self._text)

    def get_text_width(self, text):
        metric = QFontMetrics(self.font())
        return metric.width(text)

    def resizeEvent(self, e):
        super().resizeEvent(e)

        text_width = self.get_text_width(self.text)
        margins = self.contentsMargins().left() + self.contentsMargins().right()
        content_width = e.size().width()-margins

        if content_width <  text_width:
            text = self.text

            if content_width<= self.get_text_width('..'):
                self._text = '..'
                return

            for i in range(len(self.text) +1):
                textform = self.text[:len(self.text)-i]+'..'
                if self.get_text_width(text) <= content_width:
                    self._text = text
                    return

        self._text = self.text


class Label(AbstractWidget, QLabel):
    clicked = Signal()
    double_clicked = Signal()
    def __init__(self, parent=None, text='', tag=None, **kargs):
        QLabel.__init__(self, text=text)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self._pos = QPointF(0,0)
        self._time = QTime()
        self._time.start()
        #self.interval = QApplication.instance().doubleClickInterval()

    def mousePressEvent(self, e):
        self._pos = e.pos()
        self._time.restart()

        super().mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        if e.button() == Qt.LeftButton:
            dist = QLineF(self._pos, e.pos()).length()
            if dist<2:
                self.clicked.emit()

        super().mouseReleaseEvent(e)

    def mouseDoubleClickEvent(self, e):
        super().mouseDoubleClickEvent(e)
        self.double_clicked.emit()


class TextLabel(Label):
    def __init__(self, parent=None, text = '', align=('Left', 'VCenter'), size=(None, 24),
    wordWrap=False, tag=None, **kargs):
        super().__init__(parent, text=text, size=size, tag=tag, align=align, **kargs)

        #self.setIndent(1)
        self.setWordWrap(wordWrap)
        #self.setMinimumWidth(1)

class TextEdit(AbstractWidget, QTextEdit):
    def __init__(self, parent=None, text='', tag=None, **kargs):
        QTextEdit.__init__(self, parent, text=text)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.viewport().setAutoFillBackground(False)

        
'''
class PlainTextLabel(ScrollArea):
    def __init__(self, parent=None, text='', align='Left', tag=None, **kargs):
        super().__init__(parent=parent, Background='very_dark')

        #self.frame = VWidget(self, Background='very_dark', sizePolicy=('Expanding', 'Minimum'))
        

        self.label = TextLabel(self, text=text, wordWrap=True, 
        align=('Left', 'Top'), size=(None, None), sizePolicy=('Expanding', 'Fixed'), **kargs)
        self.label.setMargin(5)

        #self.frame.layout().setSizeConstraint(QLayout.SetMinimumSize)
        self.setWidget(self.label)
        self.setWidgetResizable(True)

        #self.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        #self.setMaximumHeight(400)
        #self.frame.setMinimumHeight(24)

        #self.setIndent(1)
        #self.setMinimumWidth(1)



class Image(QImage):
    def __init__(self, image, alpha, saturation = 100):
        super().__init__(Path(image).as_posix())

        p = QPainter()
        p.begin(self)
        p.setCompositionMode(QPainter.CompositionMode_DestinationIn)
        p.fillRect(self.rect(), QColor(0, 0, 0, alpha))
        p.end()


class Icon(QIcon):
    def __init__(self, icon_path):
        super().__init__()

        normal_image = Image(icon_path, alpha=175)
        active_image = Image(icon_path, alpha=250)

        self.normal_pixmap = QPixmap().fromImage(normal_image)
        self.active_pixmap = QPixmap().fromImage(active_image)

        self.addPixmap(self.normal_pixmap, state=QIcon.Off)
        self.addPixmap(self.active_pixmap, state=QIcon.On)
'''

class ImageLabel(Label):
    def __init__(self, parent=None, tag=None, **kargs):
        super().__init__(parent, tag=tag, **kargs)


class SpinBox(AbstractWidget, QSpinBox):
    def __init__(self, parent=None, value=0, min=0, max=100, step=1,
    tag=None, **kargs):
        kargs = {'Height': 24, 'Rounded': 'normal',
        'FontWeight':'light', **kargs}

        QSpinBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.setSingleStep(step)
        #self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setAlignment(Qt.AlignHCenter)

        self.setRange(min, max)
        self.setValue(value)

    def wheelEvent(self, e):
        if self.hasFocus():
            super().wheelEvent(e)
        else:
            e.ignore()


class DoubleSpinBox(AbstractWidget, QDoubleSpinBox):
    def __init__(self, parent=None, value=0, min=0, max=100, decimals=2,
    step=0.01, tag=None, **kargs):
        kargs = {'Height': 24, 'Rounded': 'normal',
        'FontWeight':'light', **kargs}

        QDoubleSpinBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.setDecimals(decimals)
        self.setSingleStep(step)
        self.setAlignment(Qt.AlignHCenter)
        #self.setSizePolicy(QSizePolicy.Fixedself.csv = PathLineEdit()
    def wheelEvent(self, e):
        if self.hasFocus():
            super().wheelEvent(e)
        else:
            e.ignore()


class LabeledWidget(HWidget):
    def __init__(self, parent=None, name='', widget=None, tag=None, **kargs):
        super().__init__(parent, tag=tag, **kargs)

        self.layout.setSpacing(5)
        self.label = WidgetLabel(self, name)
        self.layout.addWidget(widget)


class CheckBox(AbstractWidget, QCheckBox):
    def __init__(self, parent=None, name='', value=True, direction='RightToLeft', icon=None,
    sizePolicy=('Expanding', 'Fixed'), focus='StrongFocus', tag=None, **kargs):
        kargs= {'Height': 24, **kargs}

        #print('name', name)
        QCheckBox.__init__(self, name)
        AbstractWidget.__init__(self, parent, sizePolicy=sizePolicy,
        focus=focus, tag=tag, **kargs)

        self.setLayoutDirection( getattr(Qt, direction) )
        self.setChecked(value)
        self.setIcon( Icon(icon) )

    '''
    def mousePressEvent(self, event):
        super().mousePressEvent(event)

        if self.hitButton(event.pos()):
            self.setChecked( not self.isChecked() )
        '''

class ComboBox(AbstractWidget, QComboBox):
    def __init__(self, parent=None, items=[], connect=None,
    focus='StrongFocus', tag=None, **kargs):
        kargs= {'Height': 24, 'Rounded': 'normal', **kargs}

        QComboBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.setFocusPolicy(getattr(Qt, focus))

        view = QListView()
        self.setView(view)

        #self.items = items
        #if items:
        self.addItems(items)

        if connect:
            self.currentTextChanged.connect(connect)

    def clear(self):
        for i in range(self.count()):
            self.removeItem(0)

    #def addItems(self, items):
    #    for i in items:
    #        self.addItem(text=i)

    def addItem(self, icon=None, text=''):
        if icon:
            super().addItem(icon, text)
        else:
            super().addItem(text)

    def items(self):
        return [self.itemText(i) for i in range(self.count())]

    def sizeHint(self):
        size = super().sizeHint()
        size.setWidth(size.width()+2)
        return size

    def mousePressEvent(self, e):
        super().mousePressEvent(e)
        e.ignore()
        #print(self)


class PushButton(AbstractWidget, QPushButton):
    clicked = Signal(Qt.KeyboardModifiers)
    value_changed = Signal()

    def __init__(self, parent=None, text='', icon=None, size=None, sizePolicy=('Expanding','Fixed'), 
    iconSize=None, connect=None, cursor=None, toolTip='', emboss=True, checkable=False, tag=None, **kargs):
        kargs = {'Rounded': 'normal', **kargs}

        QPushButton.__init__(self, text=text)
        AbstractWidget.__init__(self, parent, toolTip=toolTip, cursor=cursor, 
        size=size, tag=tag, **kargs)

        self.modifiers = Qt.KeyboardModifiers

        self.setCheckable(checkable)

        self.defaultIcon = None

        if icon:
            self.setIcon(Icon(icon))
            
        if iconSize :
            self.setIconSize(QSize(iconSize, iconSize))
        
        if connect:
            self.clicked.connect(connect)

        if icon and not text:
            self.sizePolicy().setHorizontalPolicy(QSizePolicy.Fixed)
            self.sizePolicy().setWidthForHeight(True)
        #    self.setProperty('Width', self.property('Height'))
        #else:
        #    self.setProperty('Hpadding', 'small')

        '''
        self.shadow = QGraphicsDropShadowEffect()
        self.shadow.setBlurRadius(2)
        self.shadow.setXOffset(2)
        self.shadow.setYOffset(2)
        self.shadow.setColor(QColor(0,0,0,shadowOpacity*255))

        self.setGraphicsEffect(self.shadow)

        '''
    def setIcon(self, icon):
        if not isinstance(icon, QIcon):
            icon = Icon(icon)

        super().setIcon(icon)

        if self.defaultIcon is None:
            self.defaultIcon = icon

    def mousePressEvent(self, e):
        super().mousePressEvent(e)
        #self.setDown(True)
        self.modifiers = e.modifiers()

    def mouseReleaseEvent(self, e):
        self.blockSignals(True)
        super().mouseReleaseEvent(e)
        self.blockSignals(False)
        #self.setDown(False)
        if self.underMouse():
            self.clicked.emit(self.modifiers)
        

class Menu(QMenu):
    def __init__(self, parent=None, tag=None, **kargs):
        kargs = {'Background': 'dark', **kargs}

        QMenu.__init__(self, parent)
        AbstractWidget.__init__(self, tag=tag, **kargs)

    def addWidget(self, widget):
        action = QWidgetAction(self)
        action.setDefaultWidget(widget)
        self.addAction(action)


class WidgetAction(QWidgetAction):
    def __init__(self, menu, icon=None, text='', connect=None):
        super().__init__(menu)

        widget = PushButton(text=text, icon=icon)
        self.setDefaultWidget(widget)

        menu.addAction(self)

        if connect:
            widget.clicked.connect(connect)

        widget.clicked.connect(menu.close)


class LineEdit(AbstractWidget, QLineEdit):
    def __init__(self, parent=None, text='', size=None, place_holder=None, connect=None, tag=None, **kargs):

        QLineEdit.__init__(self, text)
        AbstractWidget.__init__(self, parent, size=size, tag=tag, **kargs)

        if place_holder:
            self.setPlaceholderText(place_holder)

        if connect:
            self.textChanged.connect(connect)

    def clear(self):
        return QLineEdit.clear(self)


class Splitter(AbstractWidget, QSplitter):
    def __init__(self, parent=None, orientation='Horizontal', tag=None, **kargs):
        QSplitter.__init__(self, getattr(Qt, orientation) )
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)


class PlainTextEdit(AbstractWidget, QPlainTextEdit):
    #click = Signal()
    #focus_out = Signal()
    plainTextChanged = Signal(str)

    def __init__(self, parent=None, textInteraction='TextEditable', 
    text='', focus='StrongFocus', readOnly=False, tag=None, **kargs):
        kargs = {'Rounded': 'normal', **kargs}

        QPlainTextEdit.__init__(self)
        AbstractWidget.__init__(self, parent, focus=focus, tag=tag, **kargs)

        self._pos = QPointF(0,0)
        self.document().setDocumentMargin(2)

        self.setReadOnly(readOnly)

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        #self.setTextInteractionFlags( getattr(Qt, textInteraction) )
        self.setPlainText(text)
        self.textChanged.connect(lambda: self.plainTextChanged.emit(self.toPlainText()) )

    def setPlainText(self, text):
        super().setPlainText(text)
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.End, QTextCursor.MoveAnchor, 1)

    def deselectAll(self):
        cursor = self.textCursor()
        cursor.clearSelection()
        self.setTextCursor(cursor)

    '''
    def mousePressEvent(self, e):
        super().mousePressEvent(e)
        if e.button() == Qt.LeftButton:
            self._pos = e.pos()
        if not self.rect().contains(e.pos()):
            w = QApplication.activeWindow()
            pos = self.viewport().mapTo(w, e.pos())
            if w.rect().contains(pos):
                self.focus_out.emit()

    def mouseReleaseEvent(self, e):
        super().mouseReleaseEvent(e)
        if e.button() == Qt.LeftButton:
            dist = QLineF(self._pos, e.pos()).length()
            if dist<2:
                self.click.emit()
    '''

class PlainTextLabel(TextEdit):
    def __init__(self, parent=None, text='', margin=(6, 2), tag=None, **kargs):
        super().__init__(parent, text=text, **kargs)
        #AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        self.setTextInteractionFlags(Qt.TextSelectableByMouse)

        if isinstance(margin, (float, int)):
            margin = [margin]*4
        elif isinstance(margin, (list, tuple)) and len(margin)==2:
            margin = margin*2

        self.setViewportMargins(*margin)

    def focusOutEvent(self, e):
        super().focusOutEvent(e)
        cursor = self.textCursor()
        cursor.clearSelection()
        #cursor.movePosition( QTextCursor.End )
        #self.setTextCursor( cursor )

    def minimumSizeHint(self):
        size = self.document().size().toSize()
        width, height = size.width(), size.height()
        return QSize(width, 24)

    def sizeHint(self):
        margin = self.viewportMargins()
        hmargin = margin.left() + margin.right()
        vmargin = margin.top() + margin.bottom()

        size = self.document().size().toSize()
        width, height = size.width(), size.height()
        return QSize(width, min(height+vmargin+2, 143))

    def resizeEvent(self, event):
        self.updateGeometry()
        super().resizeEvent(event)


class HSlider(AbstractWidget, QDoubleSpinBox):
    clicked = Signal()

    def __init__( self, parent=None, name='', value=0, min=None, max=None,
        use_offset=False, decimals=2, step=0.1, unit='', tag=None, **kargs):

        QDoubleSpinBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.min = min
        self.max = max
        self.step = step
        self.use_offset = use_offset
        self.unit = unit

        self.hover = False

        self._x = 0
        self._value = 0

        min = -float('inf') if self.min is None else self.min
        max = float('inf') if self.max is None else self.max

        self.setRange(min, max)
        self.setValue(value)
        self.setDecimals(decimals)

        self.lineEdit().hide()
        self.setButtonSymbols(QAbstractSpinBox.NoButtons)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.layout = StackedLayout(self, mode='StackAll')

        self.text_frame = Frame(self.layout)
        self.text_layout = HLayout(self.text_frame)

        #self.setProperty('BaseWidth', True)
        #self.setMinimumWidth(96)
        self.setMinimumWidth(1)
        #self.label_text.setGraphicsEffect(Shadow(self))

        if name:
            self.label_text = TextLabel(self.text_layout, name)
            self.text_layout.addStretch()

        self.label_value = TextLabel(self.text_layout)

        if not name:
            self.label_value.setAlignment(Qt.AlignCenter| Qt.AlignVCenter)
        #self.label_value.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        #self.label_value.setGraphicsEffect(Shadow(self))

        self.slider_frame = Frame(self.layout, tag = 'SliderFrame')
        #self.slider_frame.setGraphicsEffect(Shadow(self, color = 30, radius =4))

        #self.background = Frame(self.layout, tag = 'WidgetBackground')

        self.valueChanged.connect(self.update)

        self.update()
        #self.background.setProperty('ArrayIndex','first')

    def set_array_index(self,v):
        self.setProperty('ArrayIndex',v)


    '''
    @property
    def name(self):
        self.label_text.text()

    @name.setter
    def name(self, name):
        self.label_text.setText(name)
    '''
    def minimumSizeHint(self):
        return QSize()

    def sizeHint(self):
        return QSize()

    def update(self):
        self.label_value.setText(self.lineEdit().text()+' '+self.unit)

        if self.min is None or self.max is None:
            w = 0
        else:
            pos = (self.value()-self.minimum()) / (self.maximum() - self.minimum())
            w = pos*self.width()

        self.slider_frame.setMask(QRegion(-1,0,w+1,self.height()))

    def resizeEvent(self, e):
        super().resizeEvent(e)
        self.update()

    def mousePressEvent(self, event):
        self._x = event.x()
        self._value = self.value()

    def mouseMoveEvent(self, event):
        offset = (event.x()-self._x)

        if self.min is not None and self.max is not None:
            pos = (offset/self.width()) * (self.maximum() - self.minimum())
        else:
            coeff = 1 if self.decimals() == 0 else 0.1
            pos = offset *coeff

        step_offset = (pos%self.step)
        if round(step_offset, self.decimals() ) != self.step:
            pos = pos - (pos%self.step)

        self.setValue(self._value + pos)
        self.valueChanged.emit(self.value())


    def mouseReleaseEvent(self, e):
        super().mouseReleaseEvent(e)
        if e.button() == Qt.LeftButton:
            dist = QLineF(self._x, 0, e.x(), 0).length()
            if dist<2:
                self.clicked.emit()

'''
class ListWidgetItem(CheckBox):
    pressed = Signal()
    def __init__(self, list_widget, text = '', tag=None, props={}):
        self.props = {'FontWeight':'normal', 'FontSize':'normal',
        'Height': 'normal', 'Rounded': 'normal', **props}

        super().__init__(name=text, props=self.props)

        self.list_widget = list_widget
        self.pressed.connect(lambda: list_widget.setActiveWidget(self))

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

    def mousePressEvent(self, e):
        if e.button() is Qt.LeftButton:
            self.pressed.emit()

    def hitButton(self, pos):
        return True



class ListItemSmall(ListWidgetItem):
    def __init__(self, list_widget, text = '', tag=None, props={}):
        self.props = {'FontWeight':'light', 'FontSize':'small',
        'Height': 'very_small', **props}

        super().__init__(list_widget, text=text, tag=tag, props=self.props)


class ListWidget(Frame):
    index_changed = Signal(int)
    def __init__(self, parent=None, items=None, orientation='vertical', tag=None, props={}):
        super().__init__(parent, tag, props)

        if orientation == 'vertical':
            self.layout = VLayout(self)
            #self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        if orientation == 'horizontal':
            self.layout = HLayout(self)
            self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)

        self.widgets = []
        self.layout.setAlignment(Qt.AlignTop)
        self.layout.setAlignment(Qt.AlignLeft)

        if items:
            for i in items:
                self.addWidget(i)

        self.setActiveIndex(0)

    def clear(self):

        lyt = self.layout
        for i in reversed(range(lyt.count())):
            lyt.itemAt(i).widget().setParent(None)

        self.widgets = []

    def activeWidget(self):
        return [w for w in self.widgets if w.isChecked()][0]

    def setActiveWidget(self, widget):
        i = self.widgets.index(widget)
        self.setActiveIndex(i)

    def setActiveIndex(self, index):
        for i, w in enumerate(self.widgets):
            w.setChecked(index==i)

        self.index_changed.emit(index)

    def addWidget(self, text, props={}):
        w = ListWidgetItem(self, text, props)
        self.widgets.append(w)
        self.layout.addWidget(w)
'''

class ComboBoxItem(PushButton):
    def __init__(self, parent=None, icon='', text='', toolTip=''):
        super().__init__(parent, icon=icon, text=text, checkable=True,
        sizePolicy=('Expanding', 'Fixed'), toolTip=toolTip, focus='NoFocus')

        #self.setObjectName('ComboBoxItem')

        self.text = text
        #self.modifier = ()

    def mousePressEvent(self, e):
        super().mousePressEvent(e)
        self.modifier = e.modifiers()
        self.toggle()
        self.pressed.emit()
        #print(self.isChecked())


class ProgressBar(AbstractWidget, QProgressBar):
    def __init__(self, parent=None, maximum=100, textVisible=False,
    orientation='horizontal', tag=None, **kargs):
        QProgressBar.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.setMaximum(maximum)
        self.setTextVisible(textVisible)

'''
class ComboBoxExpand(Frame):
    currentTextChanged = Signal(str)
    def __init__(self, parent=None, items=[], orientation='horizontal',
    focus='StrongFocus', connect=None, tag=None, **kargs):

        super().__init__(parent=parent)

        if orientation == 'horizontal':
            self.layout = HLayout(self, spacing=10)

        elif orientation == 'vertical':
            self.layout = VLayout(self, spacing=10)

        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setFocusPolicy(getattr(Qt, focus))
        #self.layout.setAlignment(Qt.AlignHCenter)
        self.items = []
        self.active_item = None

        for w in items:
            self.addItem(text=w)

        if connect:
            self.currentTextChanged.connect(connect)

    def clear(self):
        for item in self.items:
            item.setParent(None)
            self.items.remove(item)

    def item(self, name):
        items = [i for i in self.items if i.text == name]
        if not items:
            print('The item', name, 'is not in', self.items)
            return
        return items[0]

    def currentText(self):
        current_item = self.currentItem()
        return current_item.text if current_item else None

    def currentItem(self):
        return next( (i for i in self.items if i.isChecked()), None)

    def addItems(self, items):
        for i in items:
            self.addItem(text=i)

    def addItem(self, icon=None, text=''):
        #if isinstance(item, str):
        item = ComboBoxItem(self, text=text)

        if not self.currentItem():
            self.setCurrentItem(item)

        item.pressed.connect(lambda: self.setCurrentItem(item))
        self.items.append(item)
        #self.layout.addWidget(item)

    def setCurrentItem(self, item):
        if not item: return

        if self.active_item:
            self.active_item.setChecked(False)

        self.active_item = item
        item.setChecked(True)

        self.currentTextChanged.emit(item.text)

    def setCurrentText(self, text):
        self.setCurrentItem( self.item(text) )
'''

class ListWidgetItem(QListWidgetItem):
    def __init__(self, parent, text='', icon=None, widget=None, 
    editable=False, visible=True, tag=None, **kargs):
        super().__init__(text)
        #AbstractWidget.__init__(self, tag=tag, **kargs)
        self._text = text

        
        parent.setItem(self)

        self.setHidden(not visible)

        if editable:
            self.setFlags(self.flags() | Qt.ItemIsEditable)

        if widget:
            self.setWidget(widget)       

    def __lt__(self, other):
        #print('SORT ITEM', self)
        lw = self.listWidget()
        return lw.sortKey(self) < lw.sortKey(other)

    def text(self):
        w = self.widget()
        if w and hasattr(w, 'text'):
            return super().text()
        else:
            return self._text
    
    def setText(self, text):
        self._text = text

    def setWidget(self, widget):
        self.listWidget().setItemWidget(self, widget)
        super().setText('')

    def removeWidget(self):
        self.listWidget().removeItemWidget(self)
        
    def widget(self):
        if not self.listWidget():
            return

        return self.listWidget().itemWidget(self)


class ListWidget(QListWidget, AbstractWidget):
    def __init__(self, parent=None, items=[], selectionMode='SingleSelection',
    tag=None, **kargs):
        QListWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.setSizeAdjustPolicy(QListWidget.AdjustToContents)
        self.setSelectionMode(getattr(QAbstractItemView, selectionMode) )

        self.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)

        for i in items:
            self.addItem(i)

    def sortKey(self, item):
        return item.text()

    def currentText(self):
        if self.currentItem():
            return self.currentItem().text()

    def selectedText(self):
        return [i.text() for i in self.selectedItems()]

    def setCurrentFromText(self, text):
        item = self.getItem(text)
        if item:
            self.setCurrentItem(item)

    def setSelectedFromText(self, texts):
        for i in self.items():
            i.setSelected(i.text() in texts)

    def setCurrentWidget(self, widget):
        self.clearSelection()
        item = next((i for i in self.items() if i.widget() is widget), None)
        if item:
            self.setCurrentItem(item)

    def currentWidget(self):
        if self.currentItem():
            return self.currentItem().widget()

    def widgets(self):
        return [i.widget() for i in self.items()]

    def selectedWidgets(self):
        widgets = [i.widget() for i in self.selectedItems() if not i.isHidden()]
        return [w for w in widgets if w]

    def removeWidget(self, widget):
        for i in range(self.count()):
            item = self.item(i)
            if self.itemWidget(item) == widget:
                self.takeItem(i)

    def getItem(self, name):
        return {i._text: i for i in self.items()}.get(name)

    def setItemWidget(self, item, widget):
        if widget:
            #width, height = widget.sizeHint().width(), widget.sizeHint().height()
            #item.setSizeHint( QSize(width, 0) )
            item.setSizeHint(widget.sizeHint())

            #print(widget.sizeHint(), widget.height(), widget)

            widget.item = item
        super().setItemWidget(item, widget)

        self.adjustSize()

    def onItemChanged(self, i, j):
        item = i if i else j
        self.value_changed.emit(item.name)

    def setItem(self, item):
        return super().addItem(item)

    def addItem(self, text='', editable=False, widget=None, visible=True):
        item =  ListWidgetItem(self, text=text, editable=editable, widget=widget, visible=visible)
        if not self.currentItem():
            self.setCurrentItem(item)
        
        return item
        #item.setFlags(item.flags() | Qt.ItemIsEditable)
        #super().addItem(item)

    def items(self):
        return [self.item(i) for i in range(self.count() )]

    def visibleItems(self):
        return [i for i in self.items() if not i.isHidden()]


class VListWidget(ListWidget):
    def __init__(self, parent=None, items=[], selectionMode='SingleSelection', 
    tag='EntityList', spacing=0, resizeItem=False, **kargs):
        super().__init__(parent, items=items, selectionMode=selectionMode,
        tag=tag, **kargs)

        self.setAlternatingRowColors(True)

        self.setSizeAdjustPolicy(QListWidget.AdjustToContents)
        self.setSelectionMode(getattr(QAbstractItemView, selectionMode) )

        self.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        
        for i in items:
            self.addItem(i)

        self.resizeItem = resizeItem

    def resizeEvent(self, e):
        super().resizeEvent(e)

        if not self.resizeItem:
            return

        width, height = self.viewport().width(), self.viewport().height() 


        for i in self.items():
            if not i.widget():
                continue

            size = i.widget().sizeHint()
            i.setSizeHint(QSize(width, size.height()))

        self.updateGeometry()

class GridListWidget(ListWidget):
    def __init__(self, parent=None, items=[], selectionMode='SingleSelection', itemSize=(64,64),
    tag='EntityList', **kargs):
        super().__init__(parent, items=items, selectionMode=selectionMode,
        tag=tag, **kargs)

        self.setResizeMode(QListView.Adjust)

        #self.setModelColumn(2)
        
        self.setSpacing(1)
        #self.setLayoutMode(QListView.Batched)
        self.setUniformItemSizes(True)

        self.itemSize = itemSize

        self.setMovement(QListView.Static)
        #print(self.horizontalOffset())
        #self.setFlow(QListView.LeftToRight)
        self.setViewMode(QListView.IconMode)

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        #self.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def resizeEvent(self, e):
        super().resizeEvent(e)

        if not self.count():
            return

        spacing = self.spacing()
        itemWidth, itemHeight = self.itemSize
        w, h = e.size().width(), e.size().height()
        #w, h = self.viewport().width(), self.viewport().height()

        hcount = (w + spacing) // (itemWidth + spacing)
        hcount = min(hcount, self.count())

        if not hcount:
            return

        vcount = int(self.count() / hcount)

        if (h-spacing) < (vcount*(itemHeight+spacing)):
            self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        else:
            self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        w, h = self.viewport().width(), self.viewport().height()
        
        itemWidth = ((w-0.001) - (hcount+1)*spacing) / hcount

        for item in self.items():
            item.setSizeHint(QSize(itemWidth, itemHeight))

        

class ListCompleter(VListWidget):
    def __init__(self, widget, parent=None, items=[]):
        super().__init__(items=items, tag='Completer')

        self.setParent(parent or widget)

        self.setWindowFlags(Qt.Popup)
 
        self.widget = widget

        self.setFocusPolicy(Qt.NoFocus)
        self.setFocusProxy(widget)

        self.setAttribute(Qt.WA_ShowWithoutActivating)
        self.setWindowFlags(Qt.Tool | Qt.FramelessWindowHint)
        #self.completer.setParent(self)

        #self.show()
        self.itemClicked.connect( 
           lambda x: self.widget.setText(x.text()) )
        self.itemClicked.connect( self.close )

        widget.textChanged.connect(self.filter)
        widget.installEventFilter(self)

        print('Init Completer', widget)
        self.setSizePolicy (QSizePolicy.Ignored, QSizePolicy.Preferred)
        #self.setMinimumWidth(20)
        #self.setMinimumHeight(20)
        #self.setSizeAdjustPolicy(QListWidget.AdjustToContents)
        
    #def items(self):
    #    return [self.item(i) for i in range(self.count())]
    
    def visibleItems(self):
        return [i for i in self.items() if not i.isHidden()]

    def previousItem(self):
        visibleIndex = [self.row(i) for i in self.visibleItems()]

        index = self.currentRow()
        if index == -1 or self.item(index).isHidden():
            self.setCurrentRow(visibleIndex[-1])
        else:
            index = visibleIndex.index(index) - 1
            self.setCurrentRow(visibleIndex[index])
   
    def nextItem(self):
        visibleIndex = [self.row(i) for i in self.visibleItems()]

        index = self.currentRow()
        if index==-1 or self.item(index).isHidden():
            self.setCurrentRow(visibleIndex[0])
        else:
            index = visibleIndex.index(index) + 1
            if index == len(visibleIndex):
                index = 0
            self.setCurrentRow(visibleIndex[index])

    def eventFilter(self, ob, e):
        if e.type() == QEvent.KeyPress:
            if e.key() == Qt.Key_Up:
                self.previousItem()
            elif e.key() == Qt.Key_Down:
                self.nextItem()
            elif e.key() in (Qt.Key_Enter, Qt.Key_Return):
                item = self.currentItem()
                if item and not item.isHidden():
                    self.widget.setText(self.currentItem().text())
                    self.widget.clearFocus()
                self.close()
            elif e.key() == Qt.Key_Escape:
                self.close()
        elif e.type() == QEvent.FocusIn:
            self.show()
        elif e.type() == QEvent.FocusOut:
            self.close()

        return False

    def filter(self):
        #self.clear()
        text = self.widget.text().lower()
        for item in self.items():
            item.setHidden(text not in item.text().lower() and len(text))
        
        if not self.visibleItems():
            return self.close()

        if self.hasFocus() and not self.isVisible():
             return self.show()

    #def sizeHintForColumn(self, index):
    #    return 50
    def sizeHint(self):
        #height = [i.sizeHint().height() for i in self.visibleItems]
        return QSize(self.parent().width(), super().sizeHint().height())

    def show(self):
        items = self.visibleItems()
        if len(items) == 1 and items[0].text() == self.widget.text():
            print('Ignore')
            return self.close()

        #self.setMinimumWidth(self.sizeHintForColumn(0))
        point = self.parent().rect().bottomLeft()
        global_point = self.parent().mapToGlobal(point)
        #self.setFixedWidth(self.parent().width())
        self.adjustSize()
        self.move(global_point)

        print('Show')
        super().show()
    '''
    def focusInEvent(self, e):
        print('focus in')
        self.show()
        super().focusInEvent(e)
    '''

class TableWidgetItem(QTableWidgetItem):
    def __init__(self, parent, row, column, text='', icon=None, widget=None, 
    tag=None, **kargs):
        super().__init__(text)
        
        if row == -1:
            parent.setHorizontalHeaderItem(column, self)
        elif column == -1:
            parent.setVerticalHeaderItem(row, self)
        else:
            parent.setItem(row, column, self)

        if widget:
            self.setWidget(widget)     

        if icon:
            self.setIcon( Icon(icon) )

        self._text = text

    def text(self):
        return self._text
    
    def setText(self, text):
        self._text = text

    def setWidget(self, widget):
        self.tableWidget().setCellWidget(self.row(), self.column(), widget)
        super().setText('')

    def widget(self):
        return self.tableWidget().cellWidget(self.row(), self.column())


class TableWidget(AbstractWidget, QTableWidget):
    def __init__(self, parent=None, tag=None, **kargs):
        QTableWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

    def addItem(self, row, column, text='', icon=None, widget=None):
        item =  TableWidgetItem(self, row, column, text=text, icon=icon, widget=widget)
        return item


class Dialog(QDialog, AbstractWidget):
    def __init__(self, parent=None, size=None, title='', icon=None,
    sizePolicy=None, windowFlags=[], tag=None, **kargs):
        kargs = {'Background':'dark', **kargs}

        QDialog.__init__(self, parent)
        AbstractWidget.__init__(self, sizePolicy=sizePolicy, tag=tag, **kargs)

        if size:
            self.resize(*size)

        self.setWindowTitle(title)
        self.setWindowIcon(Icon(icon) )

        '''
        flags = self.windowFlags()
        for f in windowFlags:
            flags= flags|getattr(Qt, f)
        self.setWindowFlags(flags)

        '''

class HDialog(Dialog):
    def __init__(self, parent=None, size=None, title='', icon=None, spacing=0,
    margin=0, sizePolicy=None, align='Top', tag=None, **kargs):
        super().__init__(parent, size=size, title=title, icon=icon,
        sizePolicy=sizePolicy, tag=None, **kargs)

        HLayout(self, spacing=spacing, margin=margin, align=align)


class VDialog(Dialog):
    def __init__(self, parent=None, size=None, title='', icon=None, spacing=0,
    margin=0, sizePolicy=None, align='Top', tag=None, **kargs):
        super().__init__(parent, size=size, title=title, icon=icon,
        sizePolicy=sizePolicy, tag=None, **kargs)

        VLayout(self, spacing=spacing, margin=margin, align=align)


class ListLabel(QScrollArea, AbstractWidget):
    def __init__(self, parent=None, items=[], align='Center', tag=None, **kargs):
        kargs = {'Background':'normal', 'Rounded':'normal', **kargs}

        QScrollArea.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, **kargs)

        self.widget = VWidget(align=('HCenter', 'Top'), Background='transparent', Margin='small')
        #self.widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.setWidget(self.widget)
        self.setWidgetResizable(True)
        self.align = align

        self.items = []

        for i in items:
            self.addItem(i)

    def addItem(self, text):
        label = TextLabel(self.widget, text=text, align= self.align,
        FontSize=12, FontWeight='light', FontColor='disabled',
        Margin=2, Height=18)

        self.items.append(label)

        self.widget.updateGeometry()

        return label

    def clear(self):
        lyt = self.widget.layout()
        for i in reversed(range(lyt.count())):
            lyt.itemAt(i).widget().setParent(None)

        self.items = []




'''
class ListEnumItem(PushButton):
    def __init__(self, parent=None, text='', icon=None, connect=None,
    cursor=None, toolTip='', tag=None, **kargs):

        super().__init__(parent, text=text, icon=icon, connect=connect)
        QPushButton.__init__(self, text)
        AbstractWidget.__init__(self, parent, tag='ListEnum', **kargs)

        self.setCheckable(True)
'''

