
from PySide2.QtGui import QColor
from PySide2.QtCore import Signal, QObject

from .base_widgets import VWidget, HWidget, SpinBox, DoubleSpinBox, \
Frame, PushButton, ComboBox, CheckBox, PlainTextEdit, TabWidget

from .widgets import StringLineEdit, ListEnum, PathLineEdit, ColorButton, UIList, \
IntSpinbox, FloatSpinbox

from math import ceil
from pathlib import Path
import json


class PropertyGroup(QObject):
    value_changed = Signal(dict)
    def __init__(self, path=None, **kargs):
        
        self._path = path
        if path is not None:
            self._path = Path(path).with_suffix('.json')            
        
        self._properties = {}
        self._stored_values = {}

        super().__init__()

        for k, v in kargs.items():
            setattr(self, k, v)

    @classmethod
    def from_dict(cls, data, parent=None):
        prop_data = {}
        for k, v in data.items():
            if isinstance(v, dict):
                prop_data[k] = Property.from_type(**v, name=k, parent=parent)
            else:
                prop_data[k] = Property.from_type(v, name=k, parent=parent)

        return cls(**prop_data)

    def __getattribute__(self, k):
        if k.startswith('_') or k in dir(self):
            return super().__getattribute__(k)
        else:
            return self._properties[k].value

    def __setattr__(self, k, v):
        if isinstance(v, Property):
            self._properties[k] = v

            if hasattr(v , 'value_changed'):
                v.value_changed.connect(lambda: self.value_changed.emit(self.to_dict()))

            if k in self._stored_values:
                w.value = self._stored_values[k]

        elif k.startswith('_'):
            self.__dict__[k] = v

        elif k in self._properties:
            self._properties[k].value = v
        else:
            super().__setattr__(k, v)

    def __getitem__(self, k):
        return self._properties[k]

    def __setitem__(self, k, v):
        if isinstance(v, Property):
            self._properties[k] = v

            if hasattr(v , 'value_changed'):
                v.value_changed.connect(lambda: self.value_changed.emit(self.to_dict()))

            if k in self._stored_values:
                w.value = self._stored_values[k]
        else:
            self._stored_values[k] = v

    def __repr__(self):
        text = 'PropertyGroup(\n'
        for k,v in self.items():
            text+= f'    {k}: {v}\n'
        text+= ')'
        return text

    def items(self):
        return self._properties.items()

    def keys(self):
        return self._properties.keys()

    def values(self):
        return self._properties.values()

    def to_dict(self):
        return { k: v.value for k,v in self.items() }
        
    def dict(self):
        return self._properties 
            
    def load(self):
        if self._path is None:
            return
        if not self._path.exists():
            return
        try :
            return json.loads(self._path.read_text())
        except Exception as e:
            print(e)

    def norm_data(self, obj):
        if isinstance(obj, dict):
            new = {}
            for k, v in obj.items():
                new[k] = self.norm_data(v)
        elif isinstance(obj, list):
            new = []
            for v in obj:
                new.append(self.norm_data(v))
        elif isinstance(obj, Path):
            return str(obj)
        else:
            return obj

        return new

    def restore(self, data=None):
        data = data or self.load()
        if not data:
            return

        try:
            self._stored_values = data

            for k, v in data.items():
                if k is not None and k in self._properties and v is not None:
                    self._properties[k].value = v

        except Exception as e :
            print(f'Failed to restore value for {self._path}')
            print(e)

    def save(self, data=None):
        if data is None:
            data = self.to_dict()
        
        data = self.norm_data(data)

        self._path.parent.mkdir(parents=True, exist_ok=True)
        self._path.write_text(json.dumps(data, indent=4))


class Property(QObject):
    value_changed = Signal(object)
    type = None
    def __init__(self, value, name=None, connect=None, parent=None, tag=None, **kargs):
        super().__init__()

        self.name = name
        self.widgets = []
        self._value = value
        self.default_value = value

        if parent:
            self.to_widget(parent=parent, name=name, connect=connect, tag=tag, **kargs)

        if value is not None:
            self.value = value

        if connect and not parent:
            self.value_changed.connect(connect)

    @classmethod
    def from_type(cls, value=None, items=None, **kargs):
        if items is not None:
            prop = EnumProperty(value, items=items, **kargs)
        elif isinstance(value, bool):
            prop = BoolProperty(value, **kargs)
        elif isinstance(value, str):
            prop = StringProperty(value, **kargs)
        elif isinstance(value, int):
            prop = IntProperty(value, **kargs)
        elif isinstance(value, float):
            prop = FloatProperty(value, **kargs )
        elif isinstance(value , (tuple, list)):
            prop = ListProperty(value, **kargs )
        elif isinstance(value, Path):
            prop = PathProperty(value, **kargs )
        else:
            prop = StringProperty('', **kargs)

        return prop


    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self.set_widgets_value(value)

    def set_value(self, widget, value):
        return

    def set_widgets_value(self, value, widget=None):

        self._value = value
                
        for w in self.widgets:
            if w is widget: continue

            w.blockSignals(True)
            self.set_value(w, value)
            w.blockSignals(False)
        
        self.value_changed.emit(self.value)      

    '''
    def get_layout(self, parent):
        layout = parent

        if hasattr(parent,'layout') and not hasattr(parent, 'addWidget'):
            layout = parent.layout
            if callable(parent.layout):
                layout = parent.layout()

        return layout
    '''

    def to_widget(self, parent=None, widget=None, name=None, connect=None, tag=None, **kargs):
        """ if widget is going to replace it"""
        #layout = self.get_layout(parent)

        if name is None:
            #print(self, self.name, self.value)
            name = self.name

        if widget and widget in self.widgets:
            self.widgets.remove(widget)
            widget.setParent(None)

        #print('\nW', w, self._value)

        w = self.get_widget(parent, value=self.default_value, name=name, tag=tag, **kargs)

        if self._value is not None:
            try:
                self.set_value(w, self._value)
            except Exception as e:
                print(e)

        #w.destroyed.connect(lambda: print('is destoyed'))
        w.destroyed.connect(lambda: self.widgets.remove(w))

        signal = self.get_signal(w)
        signal.connect(lambda x: self.set_widgets_value(x, w) )

        self.widgets.append(w)

        if connect:
            signal.connect(connect)

        #layout.addWidget(w)

        return w


class IntProperty(Property):
    type = int
    value_changed = Signal(type)

    def __init__(self, value=0, name='', min=None, max=None, step=1, hard=False, connect=None,
    parent=None, tag=None, **kargs):

        self.min = min
        self.max = max
        self.step = step
        self.hard = hard

        super().__init__(value, name, connect, parent, tag, **kargs)

    def get_signal(self, widget):
        return widget.valueChanged

    def set_value(self, widget, value):
        return widget.setValue(value)

    def get_widget(self, parent, value, name='', tag=None, **kargs):
        return IntSpinbox(parent, name=name, min=self.min, max=self.max, step=self.step, 
        hard=self.hard, tag=tag, **kargs)


class FloatProperty(Property):
    type = float
    value_changed = Signal(type)

    def __init__(self, value=0, name='', min=None, max=None, decimals=2, step=0.01,
    connect=None, parent=None, tag=None, **kargs):
        
        self.min = min
        self.max = max
        self.decimals = decimals
        self.step = step

        super().__init__(value, name, connect, parent, tag, **kargs)

    def get_signal(self, widget):
        return widget.valueChanged

    def set_value(self, widget, value):
        return widget.setValue(value)

    def get_widget(self, parent, value, name='', tag=None, **kargs):
        return FloatSpinbox(parent, name=name, min=self.min, max=self.max,
        decimals=self.decimals, step=self.step, tag=tag, **kargs)


class ColorProperty(Property):
    type = QColor
    value_changed = Signal(type)

    def __init__(self, value=QColor(), name='', connect=None, parent=None,
    tag=None, **kargs):
        super().__init__(value, name, connect, parent, tag, **kargs)

    def get_signal(self, widget):
        return widget.colorChanged

    def set_value(self, widget, value):
        return widget.setColor(value)

    def get_widget(self, parent, value, name='', tag=None, **kargs):
        return ColorButton(parent, name=name, tag=tag, **kargs)

    def to_tuple(self):
        v = self._value
        return v.red(), v.green(), v.blue(), v.alpha()


class TextProperty(Property):
    type = str
    value_changed= Signal(type)

    def __init__(self, value='', name='', connect=None, parent=None,
    tag=None, **kargs):
        super().__init__(value, name, connect, parent, tag, **kargs)

    def get_signal(self, widget):
        return widget.plainTextChanged

    def set_value(self, widget, value):
        return widget.setPlainText(value)

    def get_widget(self, parent, value, name='', textInteraction='TextEditable', 
    readOnly=False, tag=None, **kargs):
        return PlainTextEdit(parent, name=name, textInteraction=textInteraction,
        readOnly=readOnly, tag=tag, **kargs)


class PathProperty(Property):
    type = Path
    value_changed = Signal(type)
    def __init__(self, value='', name='', file_mode='ExistingFile', connect=None,
    parent=None, tag=None, **kargs):
    
        self.file_mode = file_mode

        super().__init__(value, name, connect, parent, tag, **kargs)

    def get_signal(self, widget):
        return widget.pathChanged

    def set_value(self, widget, value):
        return widget.setPath(str(value))

    def get_widget(self, parent, value, name='', tag=None, **kargs):
        return PathLineEdit(parent, name=name, fileMode=self.file_mode, tag=tag, **kargs)


class StringProperty(Property):
    type = str
    value_changed = Signal(type)

    def __init__(self, value='', name='', connect=None,
    parent=None, tag=None, **kargs):
        super().__init__(value, name, connect, parent, tag, **kargs)

    def get_signal(self, widget):
        return widget.textChanged

    def set_value(self, widget, value):
        return widget.setText(value)

    def get_widget(self, parent, value, name='', reset=False, clear=False,
    echo_mode='Normal',tag=None, **kargs):
        return StringLineEdit(parent, text=value, name=name, reset=reset, clearBtn=clear,
        echoMode=echo_mode, tag=tag, **kargs)


class ListProperty(Property):
    type = list
    value_changed = Signal(type)

    def __init__(self, value='', name='', connect=None,
    parent=None, tag=None, **kargs):

        super().__init__(list(value), name, connect, parent, tag, **kargs)

    def get_signal(self, widget):
        return widget.itemsChanged

    def set_value(self, widget, value):
        return widget.setItems(value)

    def get_widget(self, parent, value, name='', reset=False, tag=None, **kargs):
        return UIList(parent, items=value, name=name, reset=reset, tag=tag, **kargs)


class EnumProperty(Property):
    type = list
    value_changed = Signal(object)
    items_changed = Signal(list)

    def __init__(self, value=None, name='', connect=None, parent=None, items=None,
    exclusive=True, tag=None, **kargs):

        self.items = items or []
        self.exclusive = exclusive

        if value is None and items:
            value = items[0]

        super().__init__(value, name, connect, parent, tag=tag, **kargs)

    @Property.value.getter
    def value(self):
        if self.exclusive:
            return self._value[0] if self._value else None 
        else:
            return self._value 
                
    def add_items(self, items):
        self.blockSignals(True)
        for i in items:
            self.add_item(text=i)

        self.blockSignals(False)

        self.items_changed.emit(self.items)
        self.value_changed.emit(self.value)

    def set_items(self, items):
        if not items:
            return

        if self._value is None: # Set Default item to True
            value = items.copy()
        else:
            if self.exclusive:
                value = [v for v in items if v in self._value]
            else:
                value = [v for v in items if v in self._value or v not in self.items]

        self.clear()
        self.blockSignals(True)
        self.add_items(items)
        self.blockSignals(False)

        #self.items_changed.emit(self.items)
        self.value = value

    def add_item(self, icon=None, text=''):
        if text in self.items or text is None:
            return

        for w in self.widgets:
            w.blockSignals(True)
            w.addItem(icon, text)
            w.blockSignals(False)

        if self.value is None:
            self._value = [text]

        elif not self.exclusive and text not in self._value: 
            self._value.append(text)

        self.items.append(text)
        self.items_changed.emit(self.items)

    def clear(self, widget=None):
        #self._value = self.type()
        self.items.clear()
        #self.items= []
        for w in self.widgets:
            w.blockSignals(True)
            w.clear()
            w.blockSignals(False)
        #self.items.clear()
        #self.items= []

    def get_signal(self, widget):
        if isinstance(widget, ComboBox):
            return widget.currentTextChanged
        elif isinstance(widget, ListEnum):
            return widget.valueChanged
        elif isinstance(widget, TabWidget):
            return widget.tabChanged

    def set_widgets_value(self, value, widget=None):
        if value is None:
            return
        elif isinstance(value, str):
            value = [value]
        
        #if value == self._value:
        #    return
        self.blockSignals(True)
        for v in value:
            self.add_item(text=v)
        self.blockSignals(False)
        
        #self._value = value
        #return

        super().set_widgets_value(value, widget)


    def set_value(self, widget, value):
        if self.exclusive or isinstance(widget, (ComboBox, TabWidget)):
            value = value[0] if value else None

        if isinstance(widget, ComboBox):
            return widget.setCurrentText(value)
        elif isinstance(widget, ListEnum):
            return widget.setValue(value)
        elif isinstance(widget, TabWidget):
            return widget.setCurrentTab(value)

    def get_widget(self, parent, value, name='', mode='compact',
    focus='StrongFocus', exclusive=None, tag=None, **kargs):

        if mode == 'compact':
            return ComboBox(parent, name=name, items=self.items, 
            focus=focus, tag=tag, **kargs)
        elif mode == 'expand':
            return ListEnum(parent, name=name, items=self.items, 
            exclusive=exclusive if exclusive is not None else self.exclusive, 
            focus=focus, tag=tag, **kargs)
        elif mode == 'tab':
            return TabWidget(parent, name=name, tabs=self.items,
            focus=focus, tag=tag, **kargs)


class BoolProperty(Property):
    type = bool
    value_changed = Signal(bool)

    def __init__(self, value=False, name='', connect=None, parent=None, 
    tag=None, **kargs):
        kargs = {'Rounded': 'normal', 'IconSize':'small', **kargs}

        super().__init__(value, name, connect, parent, tag=tag, **kargs)

    def get_signal(self, widget):
        return widget.stateChanged

    def set_value(self, widget, value):
        return widget.setChecked(value)

    def get_widget(self, parent, name, focus='StrongFocus', direction='RightToLeft',
    tag=None, **kargs):
        return CheckBox(parent, name=name, direction=direction, focus=focus, tag=tag, **kargs)

    def set_widgets_value(self, value, widget=None):

        value = self.type(value)

        super().set_widgets_value(value, widget)

